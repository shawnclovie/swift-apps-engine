@testable import AppsEngine
import XCTVapor

final class UtilityTests: XCTestCase {
	func testError() {
		let base = Errors.internal
		var err = base.wrap("a error description for a")
			.wrap("cancelled for test")
			.wrap("3rd error")
		XCTAssertEqual(base, err.base)
		err = err.wrap(Errors.forbidden)
		XCTAssertEqual(Errors.forbidden, err.base)
		XCTAssertTrue(err.contains(oneOf: base, .appIDEmpty))
		XCTAssertFalse(err.contains(oneOf: .database, .gateway))
		print(err.description)
		print(err.descriptionWithCallerStack)

		err = Errors.internal.convertOrWrap(AnyError("a"))
		print(err.descriptionWithCallerStack)
		
		err = Errors.database.wrap(Errors.appIDEmpty.error(extra: [
			"app_id": "z",
		]), extra: Self.jsonObject)
		print(HTTPResponse.error(headers: .init([("Accept", "gzip")]), err).bytes.string)
		print(HTTPResponse.error(headers: .init([
			("Accept", "gzip"),
			(HTTPConst.contentType, HTTPMediaType.plainText.description),
		]), err).bytes.string)
	}

	static let jsonObject: [String: JSON] = [
		"foo": [
			"name": ["foo1": "bar"],
			"nums": ["a", nil, "b", 4],
		],
		"null": nil,
		"bool": true,
		"int8": 23,
		"int64": 9223372036854775807,
		"float": 1.1,
		"double": 3.141592642,
		"array": [1, 2, false, 235990, 234, "lk23", nil],
		"arrayEmpty": [],
		"objEmpty": [:],
		"url": "https://www.google.com/?q=搜索word&foo=bar",
		"st\\ri\"ng": "tes&%\\ { 👨‍👨‍👧‍👦cancelled👩‍👩‍👦‍👦tcancelled   cancelled中国  的fql23",
	]

	func testJSON() throws {
		do {
			let num = JSON(from: 1.0211)
			XCTAssertNil(num.int64Value)
			XCTAssertEqual("1.0211", num.description)
		}
		let obj = JSON.object(Self.jsonObject)
		XCTAssertEqual("bar", obj["foo", "name", "foo1"].stringValue)
		XCTAssertEqual(1.1, obj["float"].doubleValue)
		XCTAssertEqual(JSON.null, JSON(from: nil))
		XCTAssertEqual(JSON.bool(false), JSON(from: false))
		XCTAssertEqual(JSON.number(.init(unsigned: 1)), JSON(from: 1))
		XCTAssertEqual(JSON.number(.init(unsigned: UInt64(Int64.max))), JSON(from: Int64.max))
		XCTAssertEqual(JSON.number(.init(double: 1.1)), JSON(from: 1.1))
		let abc = "abc"
		XCTAssertEqual(JSON.string(abc), JSON(from: abc))
		XCTAssertEqual(JSON.string("a"), JSON(from: abc.dropLast(2)))
		XCTAssertEqual(JSON.array([JSON.string(abc)]), JSON(from: [abc]))
		XCTAssertEqual(JSON.object([abc: JSON.string(abc)]), JSON(from: [abc: abc]))
		XCTAssertEqual(JSON.null, JSON(from: Errors.notFound))
	}
	
	func testJSONCodable() throws {
		let obj = JSON.object(Self.jsonObject)
		let encoder = JSON.Encoder(options: [.sortedKeys])
		let s = encoder.encode(obj)
		print(s)
		do {
			let encoder = JSONEncoder()
			encoder.outputFormatting = [.sortedKeys]
			let s1 = String(decoding: try encoder.encode(obj), as: UTF8.self)
			print(s1)
		}
		let value = try JSON.Decoder().decode(s.data(using: .utf8)!)
		XCTAssertEqual(s, encoder.encode(value))
	}

	var measureOptions: XCTMeasureOptions {
		let opt = XCTMeasureOptions()
		opt.iterationCount = 1000
		return opt
	}

	func testJSONMetricString() throws {
		measure(options: measureOptions) {
			let _ = JSON.Encoder(options: []).encode(JSON.object(Self.jsonObject))
		}
	}

	func testObjectStorage() throws {
		let basepath = "rest_path"
		let urlAWS = "/my_bucket/\(basepath)?name=aws&region=us-east-1&secret=AKIAZZZZZZZZZZZZDMHP:DZMIeyI30ivK3zzZZzzZZ4LITNyAAaaAAzzp1SPK"
		let urlOSS = "http://oss-cn-beijing.aliyuncs.com/my_bucket/\(basepath)?secret=LJLFo2lOE:23j32oklsd9032lkdsfoizze"
		for url in [urlAWS, urlOSS] {
			let source = try ObjectStorageSource(urlString: url)
			let source2 = try ObjectStorageSource(urlString: source.urlString)
			XCTAssertEqual(source, source2)
			XCTAssertEqual("\(basepath)/foo", source.fullpath("foo"))
		}
	}
	
	func testTimeRFC3339() {
		let text = "2020-11-2T13:00:03+02:00"
		let date = RFC3339.shared.date(from: text)
		XCTAssertNotNil(date)
		XCTAssertEqual(RFC3339.shared.string(from: date!, offset: 0), "2020-11-02T11:00:03Z")
		XCTAssertEqual(RFC3339.shared.string(from: date!, offset: 28800), "2020-11-02T19:00:03+08:00")
		let time = Time(rfc3339: text)
		XCTAssertNotNil(time)
		XCTAssertEqual(time!.offset, 7200)
		XCTAssertEqual(time!.asDate, date!)
	}
}
