import AppsEngine
import Foundation
import FluentPostgresDriver
import FluentMongoDriver
import Vapor

let resourceURL: URL = {
	let bundlePath = Bundle.main.path(forResource: nil, ofType: "bundle")!
	let bundle: Bundle = .init(path: bundlePath)!
	return bundle.resourceURL!
}()

try ServerConfig.shared.load(workingDirectory: resourceURL)
Base.resetUUID(nodeIndex: try Base.produceNodeIndexWithLanIPAndPID())
let engine = try Engine(
	config: ServerConfig.shared,
	modules: [
		// Working modules
		TestModule(),
		RouteModule(),
	],
	appUpdaterBuilder: { provider in ZippedAppConfigUpdater(provider: provider) }
)
try waitAsyncTask(priority: .high) {
try await engine.prepare(
	databaseBuilder: { (config) in
		switch config.url.scheme {
		case "postgres":
			return try DatabaseConfigurationFactory.postgres(url: config.url).make()
		case "mongodb":
			return try DatabaseConfigurationFactory.mongo(settings: .init(config.url.absoluteString)).make()
		default:
			throw AnyError("unknown database type '\(config.url.scheme as Any)'")
		}
	}
//	storageBuilder: ObjectStorageAWSS3Provider.init(source:)
//	appDetector: AppInfoHeaderAppDetector(),
//	requestProcessor: RouteModule.RequestProcessor()
)
}.get()
engine.appWillPrepareHook = { config in
	config.core.cors?.allowedHeaders.append(contentsOf: [.init("X-Foo")])
}
try engine.runServer()
