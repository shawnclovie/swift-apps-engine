import AppsEngine
import Foundation
import Fluent
import FluentSQL
import MongoKitten
import Vapor

extension Resource.Group {
	var sql: SQLDatabase { databases["sql"]! as! SQLDatabase }
	var nosql: Database { databases["nosql"]! }
}

struct TestModule: Module {
	var moduleName: String { "test" }
	
	struct T1Model: Codable {
		static let schema = "test1"
		var id: Int
		var name: String
	}

	final class T1MongoModel: Codable, Model {
		
		static let schema = "test1"
		@ID(custom: .id)
		var id: ObjectId?
		
		@Field(key: "name")
		var name: String
		
		init() {}
		
		init(id: ObjectId? = nil, name: String) {
			self.name = name
		}
	}

	var endpoints: [Endpoint] {
		let epsWS = Endpoint.Group(endpointPrefix: "test", pathPrefix: "/test", endpoints: [
			.init("ws", path: "/ws", webSocket: TestWebSocketInvocation()),
		]).endpoints
		let epsTest = Endpoint.Group(endpointPrefix: "test", pathPrefix: "/test", endpoints: [
			.init("get", .GET, path: "/", { ctx in
				let cfg = ctx.context.config.get(Config.self)!
				return .json(.ok, [
					"keys": cfg.appKeys.map({ ["name": $0.name, "key": $0.key] })
				])
			}),
			.init("match", .GET, path: "/match/**", { ctx in
				return .text(.ok, "\(ctx.request.parameters)")
			}),
			.init("echo", .POST, path: "/echo", { ctx in
				if ctx.request.headers.contains(name: "throws") {
					throw Errors.uncaught
				}
				return .binary(.ok, ctx.body ?? .init())
			}),
			.init("db", .GET, path: "/db", { ctx in
				let tableName = "test1"
				let db = Resource.shared.defaultGroup!.sql
				do {
					try await db.delete(from: tableName).run()
//					try await db.create(table: tableName).ifNotExists().column(definitions: [
//						.init("id", dataType: .int),
//						.init("name", dataType: .text),
//					]).run()
					try await db.delete(from: tableName).run()
					for i in 0..<10000 {
						try await db.insert(into: tableName)
							.columns(["id", "name"])
							.values([i, "\(i)"])
							.run()
//						try await db.raw(SQLQueryString("INSERT INTO \(tableName) (id,name) VALUES (\(i),'\(i)')")).run()
					}
					let rows = try await db.select().columns("*").from(tableName).all(decoding: T1Model.self)
					do {
						let _ = try ctx.request.query.get(at: "count") as String
						return .text(.ok, "\(rows.count)")
					} catch {}
					let bytes = try JSONEncoder().encode(rows)
					return .binary(.ok, .init(data: bytes))
				} catch {
					throw Errors.database.wrap(error)
				}
			}),
			.init("nosql", .GET, path: "/nosql", { ctx in
				let db = Resource.shared.defaultGroup!.nosql
				do {
//					_ = try db[T1MongoModel.schema].deleteAll(where: "" == "").wait()
					try await T1MongoModel.query(on: db).delete()
					try await T1MongoModel.query(on: db).delete()
					for i in 0..<10000 {
						try await T1MongoModel(name: "\(i)").create(on: db)
					}
					let rows = try await db.query(T1MongoModel.self).all()
					let bytes = try JSONEncoder().encode(rows.count)
					return .binary(.ok, .init(data: bytes))
				} catch {
					throw Errors.database.wrap(error)
				}
			}),
			// TODO: proxy as a fixed endpoint
		], middlewares: [
			ClosureMiddleware({ ctx in
				print("m1 before process")
				if ctx.request.headers.contains(name: "m1throws") {
					throw Errors.uncaught
				}
				var res = await ctx.next()
				print("m1 after process")
				res.headers.replaceOrAdd(name: "m1foo", value: "bar")
				return res
			}),
			ClosureMiddleware({ ctx in
				print("m2 before process")
				var res = await ctx.next()
				print("m2 after process")
				res.headers.replaceOrAdd(name: "m2foo", value: "bar")
				return res
			}),
		]).endpoints
		return epsWS + epsTest
	}
	
	var isSegmental: Bool { false }
	
	func parseConfig(_ configSet: AppConfigSet, segmentID: String) -> AppConfigParseResult? {
		var config = Config()
		if let rawData = configSet.core.rawData["appkeys"] as? [[String: Any]] {
			config.appKeys.append(contentsOf: rawData.map({
				AppKey(name: $0["name"] as? String ?? "", key: $0["key"] as? String ?? "")
			}))
		}
		return .init(config, warnings: [])
	}
	
	struct AppKey {
		let name: String
		let key: String
	}
	
	struct Config {
		var appKeys: [AppKey] = []
	}
}

struct TestWebSocketInvocation: WebSocketInvocation {
	func webSocketDidConnect(_ webSocket: WebSocket, on ctx: RequestContext) async throws {
		print("webSocketDidConnect()")
	}
	func webSocket(_ webSocket: WebSocket, on ctx: RequestContext, received upstream: WebSocketUpStream) async {
		print("webSocket(_:on:received:) upstream=\(upstream)")
	}
	
	func webSocket(_ webSocket: WebSocket, on ctx: RequestContext, didClose error: Error?) {
		print("webSocket(_:on:didClose:) error=\(error as Any)")
	}
}

struct RouteModule: Module {
	var moduleName: String { "route" }
	
	var endpoints: [Endpoint] { [] }
	
	func parseConfig(_ configSet: AppConfigSet, segmentID: String) -> AppConfigParseResult? {
		.init(Config())
	}
	
	struct Config {
	}
	
	struct TestRequestProcessor: RequestProcessor {
		func processRequest(_ context: Context, _ request: Request) -> ByteBuffer? {
			guard var buf = request.body.data,
				  var data = buf.readData(length: buf.readableBytes)
			else { return nil }
			data = Data(data.reversed())
			return .init(data: data)
		}
		
		func processResopnse(_ context: Context, _ response: HTTPResponse) -> HTTPResponse {
			response
		}
	}
}

struct AppInfo: StorageKey {
	typealias Value = AppInfo
	
	let appID: String
	let clientPlatform: String
	let clientVersion: Int
}

/// Detect app with AppInfo
struct AppInfoHeaderAppDetector: EngineAppDetector {
	static let headerAppInfo  = "App-Info"

	func detectApp(request: Request, in engine: Engine) -> EngineApp? {
		guard let rawInfo = request.headers.first(name: Self.headerAppInfo)
		else { return nil }
		let comps = rawInfo.split(separator: ":")
		guard comps.count >= 3
		else { return nil }
		guard let app = engine.apps.get()[comps[0].lowercased()]
		else { return nil }
		let info = AppInfo(appID: app.appID,
						   clientPlatform: String(comps[1]),
						   clientVersion: Int(comps[2]) ?? 0)
		request.storage.set(AppInfo.self, to: info, onShutdown: nil)
		return app
	}
}
