//
//  Endpoint.swift
//  
//
//  Created by Shawn Clovie on 22/6/2022.
//

import Foundation
import Vapor

public struct Endpoint {
	public enum Invocation {
		case request(RequestInvocation)
		case webSocket(WebSocketInvocation)
	}
	
	public var name: String
	public var route: Route
	public var invocation: Invocation
	
	var middlewares: [EndpointMiddleware]
	
	public init(_ name: String, _ method: HTTPMethod..., path: String, _ fn: @escaping RequestClosure, middlewares: [EndpointMiddleware] = []) {
		self.init(name, method, path: path, .request(ClosureRequestInvocation(fn)), middlewares: middlewares)
	}
	
	public init(_ name: String, _ method: HTTPMethod..., path: String, _ invocation: RequestInvocation, middlewares: [EndpointMiddleware] = []) {
		self.init(name, method, path: path, .request(invocation), middlewares: middlewares)
	}
	
	public init(_ name: String, path: String, webSocket invocation: WebSocketInvocation, middlewares: [EndpointMiddleware] = []) {
		self.init(name, [], path: path, .webSocket(invocation), middlewares: middlewares)
	}
	
	public init(_ name: String, _ method: [HTTPMethod], path: String, _ invocation: Invocation, middlewares: [EndpointMiddleware] = []) {
		route = .init(method, path)
		self.name = name
		self.invocation = invocation
		self.middlewares = middlewares
	}

	public func withMiddlewares(_ mws: EndpointMiddleware...) -> Self {
		var inst = self
		inst.middlewares.append(contentsOf: mws)
		return inst
	}
}

extension Endpoint {
	public struct Route {
		public var method: [HTTPMethod]
		public var path: String
		
		init(_ method: [HTTPMethod], _ path: String) {
			self.method = method
			self.path = path
		}
		
		var name: String { "\(method) \(path)" }
	}
}

extension Endpoint {
	public struct Group {
		private let endpointPrefix: String
		private let pathPrefix: String
		private var _endpoints: [Endpoint]
		private var middlewares: [EndpointMiddleware]
		
		public init(endpointPrefix: String, pathPrefix: String,
					endpoints: [Endpoint],
					middlewares: [EndpointMiddleware] = []) {
			self.endpointPrefix = endpointPrefix
			self.pathPrefix = pathPrefix
			_endpoints = endpoints
			self.middlewares = middlewares
		}
		
		public func withMiddlewares(_ mws: EndpointMiddleware...) -> Self {
			var inst = self
			inst.middlewares.append(contentsOf: mws)
			return inst
		}
		
		public mutating func append(endpoinds: Endpoint...) {
			_endpoints.append(contentsOf: endpoinds)
		}
		
		public mutating func append(middlewares: EndpointMiddleware...) {
			self.middlewares.append(contentsOf: middlewares)
		}

		public var endpoints: [Endpoint] {
			let epPrefix = endpointPrefix.hasSuffix(".") ? endpointPrefix : "\(endpointPrefix)."
			return _endpoints.map { ep -> Endpoint in
				var ep = ep
				ep.name = epPrefix + ep.name
				ep.route.path = joinPathComponents(pathPrefix, ep.route.path, separator: "/")
				ep.middlewares.insert(contentsOf: middlewares, at: 0)
				return ep
			}
		}
	}
	
	public struct Register {
		public internal(set) var endpoints: [String: Endpoint] = [:]
		
		public init() {}
		
		public mutating func register(_ eps: [Endpoint]) throws {
			for ep in eps {
				if endpoints[ep.name] != nil {
					throw Errors.forbidden.wrap("duplicate endpoint name")
				}
				endpoints[ep.name] = ep
			}
		}
	}
}
