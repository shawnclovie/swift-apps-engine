import Foundation
import NIO
import Vapor

public final class Engine {
	public static private(set) var verboseItems = Set<String>()
	
	private let env: Environment
	private let app: Application

	private let config: ServerConfig

	public private(set) var apps = SynchronizableValue<[String: EngineApp]>([:])

	public private(set) var appHostMapping = SynchronizableValue<[String: String]>([:])

	/// The hook could be used to change configuration for the prepared app.
	public var appWillPrepareHook: ((_ config: inout AppConfigSet) -> Void)?

	private let configProvider: AppConfigProvider
	private let appDetector: EngineAppDetector

	public var apiMetric: APIMetricRecorder = DefaultAPIMetricRecorder()

	var requestProcessor: RequestProcessor?
	var middlewares: [Vapor.Middleware] = []

	/// Create Server and prepare multi-app running environment.
	/// - Parameters:
	///   - appDetector: to detect app for response each request.
	public init(
		config: ServerConfig,
		modules: [Module],
		appUpdaterBuilder: AppConfigProvider.UpdatorBuilder? = nil,
		appDetector: EngineAppDetector = DefaultAppDetector()
	) throws {
		let verbose = ProcessInfo.processInfo.environment
			.first(where: { key, _ in key.uppercased() == Keys.envRuntimeVerbose })
		if let verbose = verbose?.value, !verbose.isEmpty {
			for it in verbose.split(separator: ",") {
				Self.verboseItems.insert(it.lowercased())
			}
			Metric.shared.verbose = Self.verboseItems.contains("metric")
		}
		var env = try Environment.detect()
		try LoggingSystem.bootstrap(from: &env)
		StackTrace.isCaptureEnabled = true

		self.env = env
		self.config = config
		app = .init(env)
		configProvider = try .init(
			modules: modules,
			appSource: config.appSource,
			appUpdaterBuilder: appUpdaterBuilder)
		self.appDetector = appDetector
	}
	
	/// Prepare resources and apps.
	/// - Parameters:
	///   - databaseBuilder: Use to prepare database connections.
	///   - storageBuilder: Use to prepare object storage.
	public func prepare(
		databaseBuilder: Resource.DatabaseBuilder? = nil,
		storageBuilder: Resource.StorageBuilder? = nil
	) async throws {
		if let res = config.resource {
			Logger.startup.log(.info, "init resource")
			try await Resource.shared.setup(
				res,
				application: app,
				databaseBuilder: databaseBuilder,
				storageBuilder: storageBuilder)
		}
		try await prepareApps()
	}

	deinit {
		app.shutdown()
		configProvider.unregister(updateHandler: self)
	}

	/// Run Server and respond request.
	/// - Parameters:
	///   - requestProcessor: to process request and response if needed.
	public func runServer(
		defaultMaxBodySize: ByteCount = "10mb",
		requestProcessor: RequestProcessor? = nil
	) throws {
		self.requestProcessor = requestProcessor

		app.http.server.configuration.address = .hostname(config.server.host, port: config.server.port)
		app.routes.defaultMaxBodySize = defaultMaxBodySize

		app.responder.use { app in
			self.middlewares = app.middleware.resolve()
			return self
		}

		let ip = (try? SocketAddress.lanAddress())?.ipAddress ?? "unknown_ip"
		Metric.shared.count("runtime.run_server.\(ip)")
		Logger.startup.log(
			.info, "runtime.run_server",
			.init("ip", ip),
			.init("pid", ProcessInfo.processInfo.processIdentifier),
			.init("config", config.server))
		try app.run()
	}

	func prepareApps() async throws {
		configProvider.register(updateHandler: self)
		try await configProvider.startUpdate()
	}

	public var appIDs: [String] { .init(apps.get().keys) }
}

extension Engine: Responder {
	public func respond(to request: Request) -> EventLoopFuture<Response> {
		guard let app = appDetector.detectApp(request: request, in: self)
		else {
			Metric.shared.count("runtime.unknown_app_request")
			Logger.default.log(.warn, "unknown_app_request", .init(Keys.host, DefaultAppDetector.extractHost(request: request) ?? ""))
			let resp = HTTPResponse.error(Errors.engineAppNotFound)
			return request.eventLoop.future(resp.response)
		}
		return app.respond(to: request)
	}
}

extension Engine: AppConfigUpdateListener {
	public func updateDidFinish(updatedAppIDs: Set<String>, removedAppIDs: Set<String>) {
		let logger = Logger.default
		var newApps: [String: EngineApp] = [:]
		for appID in updatedAppIDs {
			let appPath = configProvider.rootPath.appendingPathComponent(appID)
			let app: EngineApp
			do {
				app = try EngineApp(
					path: appPath,
					predefinedEndpoints: configProvider.endpoints,
					modules: configProvider.modules,
					requestProcessor: requestProcessor,
					apiMetric: apiMetric)
			} catch {
				logNewAppFailure(logger: logger, appID: appID, appPath: appPath, error: error, when: "update")
				continue
			}
			Metric.shared.count("\(app.config.metricName).app.new_app.ok", count: 1)
			appWillPrepareHook?(&app.config)
			app.prepare(middlewares: middlewares)
			newApps[appID] = app
		}
		let oldApps = apps.get()
		for (appID, app) in oldApps {
			if newApps[appID] == nil && !removedAppIDs.contains(appID) {
				newApps[appID] = app
			} else {
				app.dispose()
			}
		}
		apps.waitAndSet(newApps)
		updateAppHostMapping()
	}
	
	private func updateAppHostMapping() {
		var hostMapping: [String: String] = [:]
		for (appID, app) in apps.get() {
			for host in app.config.core.hosts {
				hostMapping[host] = appID
			}
		}
		appHostMapping.waitAndSet(hostMapping)
	}
	
	private func logNewAppFailure(logger: Logger, appID: String, appPath: URL, error: Error, when: String) {
		logger.log(.error, "new App failed", .appID(appID), .error(error))
		Metric.shared.count("\(appID).app.new_app.failed", count: 1)
		try? "when: \(when)\n\n\(error)".write(to: appPath, atomically: true, encoding: .utf8)
	}
}

public protocol EngineAppDetector {
	func detectApp(request: Request, in engine: Engine) -> EngineApp?
}

extension Engine {
	/// Detect app with `Host` header.
	public struct DefaultAppDetector: EngineAppDetector {
		public static func extractHost(request: Request) -> String? {
			guard let host = request.url.host ?? request.headers.first(name: .host)
			else { return nil }
			if let index = host.firstIndex(of: ":") {
				return String(host[..<index])
			}
			return host
		}
		
		public init() {}
		
		public func detectApp(request: Request, in engine: Engine) -> EngineApp? {
			guard let host = Self.extractHost(request: request),
				  let appID = engine.appHostMapping.get()[host]
			else { return nil }
			return engine.apps.get()[appID]
		}
	}
}
