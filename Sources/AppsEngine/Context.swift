import Vapor

public final class Context {
	public let configSet: AppConfigSet?
	public private(set) var config: AppConfig
	public private(set) var userID: String?
	public let traceID: String
	public let logger: Logger
	public let startTime: Time
	
	private var storage: [ObjectIdentifier: Any] = [:]
	
	var cachedResource: Resource.Group?
	
	private let lock = NSLock()

	public convenience init(config: AppConfig, userID: String?, startTime: Time) {
		self.init(configSet: nil, config: config, userID: userID, startTime: startTime)
	}

	public convenience init(configSet: AppConfigSet, userID: String?, startTime: Time) {
		self.init(configSet: configSet, config: configSet.core, userID: userID, startTime: startTime)
	}

	convenience init(configSet: AppConfigSet) {
		self.init(configSet: configSet, config: configSet.core, userID: nil,
				  startTime: Time(offset: configSet.core.timeOffset))
	}

	init(configSet: AppConfigSet?, config: AppConfig,
		 userID: String?, startTime: Time) {
		self.configSet = configSet
		self.config = config
		self.userID = userID
		traceID = SnowflakeNode.shared.generate().base36
		logger = Logger.default.with(trace: .init(traceID, on: startTime, [.appID(config.appID)]))
		self.startTime = startTime
	}
	
	public var appID: String { config.appID }
	
	public func reset(userID: String) {
		lock.withLock {
			self.userID = userID
			// TODO: switch config if needed
		}
	}

	public func get<As>(_ configType: As.Type) -> As? {
		storage[ObjectIdentifier(configType)] as? As
	}

	public func set<As>(_ value: As) {
		storage[ObjectIdentifier(type(of: value))] = value
	}
}

public protocol RequestProcessor {
	func processRequest(_ context: Context, _ request: Request) -> ByteBuffer?
	func processResopnse(_ context: Context, _ response: HTTPResponse) -> HTTPResponse
}

struct WrappedRequestError {
	var error: WrapError
	var invocation: Invocation
	var isMiddleware: Bool
}

public final class RequestContext {
	public let context: Context
	public let endpoint: Endpoint
	public let request: Request
	public let body: ByteBuffer?

	private var executor = Executor()

	public init(request: Request, context: Context, endpoint: Endpoint, body: ByteBuffer?) {
		self.request = request
		self.context = context
		self.endpoint = endpoint
		self.body = body
	}
	
	/// Get WebSocket if serving WebSocket.
	public var webSocket: WebSocket? {
		guard case let .webSocket(_, ws) = executor.invocation else {
			return nil
		}
		return ws
	}

	public func decodeAs<AsType>(_ type: AsType.Type) -> Result<AsType, WrapError>
	where AsType: Decodable {
		guard let body = body else {
			return .failure(Errors.noBody)
		}
		guard let contentType = request.headers.contentType else {
			return .failure(Errors.noContentType)
		}
		do {
			let decoder = try ContentConfiguration.global.requireDecoder(for: contentType)
			let obj = try decoder.decode(type, from: body, headers: request.headers)
			return .success(obj)
		} catch {
			let err: WrapError
			switch error {
			case let error as DecodingError:
				err = Errors.invalidParameter.wrap(error.reason)
			default:
				err = Errors.invalidParameter.wrap(error)
			}
			return .failure(err)
		}
	}

	func serve() async -> HTTPResponse {
		guard case .request(let inv) = endpoint.invocation else {
			return .error(Errors.forbidden.wrap("request not supported"))
		}
		executor = .init(middlewares: endpoint.middlewares, invocation: .request(inv))
		return await next()
	}

	func serve(webSocket: WebSocket) async {
		guard case .webSocket(let inv) = endpoint.invocation else {
			await close(webSocket: webSocket, response: .error(Errors.forbidden.wrap("websocket not supported")))
			return
		}
		executor = .init(middlewares: endpoint.middlewares, invocation: .webSocket(inv, webSocket))
		let res = await next()
		guard res.error == nil else {
			await close(webSocket: webSocket, response: res)
			return
		}
		webSocket.onClose.whenComplete { (result) in
			var err: Error?
			if case .failure(let e) = result {
				err = e
			}
			inv.webSocket(webSocket, on: self, didClose: err)
		}
		webSocket.onPing { (ws) async in
			await inv.webSocket(ws, on: self, heartbeat: .ping)
		}
		webSocket.onPong { (ws) async in
			await inv.webSocket(ws, on: self, heartbeat: .pong)
		}
		webSocket.onText { (ws, text) async in
			await inv.webSocket(ws, on: self, received: .text(text))
		}
		webSocket.onBinary { (ws, buf) in
			await inv.webSocket(ws, on: self, received: .binary(buf))
		}
	}

	public func next() async -> HTTPResponse {
		if let mw = executor.nextMiddleware() {
			let res: HTTPResponse
			do {
				res = try await mw.respond(to: self)
			} catch {
				res = processDidFailure(error: Errors.internal.convertOrWrap(error, callerSkip: 1), mw, isMiddleware: true)
			}
			executor.lastResponse = res
			return res
		}
		if executor.hadJustCalledAllMiddlewares,
		   let inv = executor.invocation {
			let res: HTTPResponse
			do {
				switch inv {
				case .request(let inv):
					res = try await inv.respond(to: self)
				case .webSocket(let inv, let webSocket):
					try await inv.webSocketDidConnect(webSocket, on: self)
					res = .empty(.ok)
				}
			} catch {
				let err = Errors.internal.convertOrWrap(error, callerSkip: 1)
				res = processDidFailure(error: err, inv.invocation, isMiddleware: false)
			}
			executor.lastResponse = res
			return res
		}
		return executor.lastResponse
			?? .error(Errors.notFound.wrap("\(endpoint) no result"))
	}
	
	private func processDidFailure(error: WrapError,
								   _ invocation: Invocation,
								   isMiddleware: Bool) -> HTTPResponse {
		let wrapped = WrappedRequestError(error: error, invocation: invocation, isMiddleware: isMiddleware)
		context.set(wrapped)
		return .error(wrapped.error)
	}
	
	func close(webSocket: WebSocket, response: HTTPResponse) async {
		var bytes = response.bytes
		let data = bytes.readString(length: bytes.readableBytes) ?? ""
		try? await webSocket.send(data)
		try? await webSocket.close(code: .goingAway)
	}
}

extension RequestContext {
	private struct Executor {
		enum Invocation {
			case request(RequestInvocation)
			case webSocket(WebSocketInvocation, WebSocket)
			
			var invocation: AppsEngine.Invocation {
				switch self {
				case .request(let inv):
					return inv
				case .webSocket(let inv, _):
					return inv
				}
			}
		}

		var middlewares: [EndpointMiddleware] = []
		var middlewareCallIndex = -1
		var invocation: Invocation?
		var lastResponse: HTTPResponse?
		
		init() {}
		
		init(middlewares: [EndpointMiddleware], invocation: Invocation) {
			self.middlewares = middlewares
			self.invocation = invocation
		}
		
		var hadJustCalledAllMiddlewares: Bool {
			middlewareCallIndex == middlewares.count
		}
		
		mutating func nextMiddleware() -> RequestInvocation? {
			let mwIndex = middlewareCallIndex + 1
			middlewareCallIndex = mwIndex
			return mwIndex < middlewares.count ? middlewares[mwIndex] : nil
		}
	}
}
