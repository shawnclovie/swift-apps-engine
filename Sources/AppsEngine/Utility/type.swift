import Foundation

public func isNumeric(_ v: Any?) -> Bool {
	switch v {
	case is Int64:		fallthrough
	case is Int:		fallthrough
	case is Int8:		fallthrough
	case is Int16:		fallthrough
	case is Int32:		fallthrough
	case is UInt:		fallthrough
	case is UInt8:		fallthrough
	case is UInt16:		fallthrough
	case is UInt32:		fallthrough
	case is UInt64:		fallthrough
	case is Double:		fallthrough
	case is Float:
		return true
	default:
		return false
	}
}

public func anyToInt64(_ v: Any?) -> Int64? {
	switch v {
	case let v as Int64:		return v
	case let v as Int:			return Int64(v)
	case let v as Int8:			return Int64(v)
	case let v as Int16:		return Int64(v)
	case let v as Int32:		return Int64(v)
	case let v as UInt:			return Int64(v)
	case let v as UInt8:		return Int64(v)
	case let v as UInt16:		return Int64(v)
	case let v as UInt32:		return Int64(v)
	case let v as UInt64:		return Int64(v)
	case let v as Double:		return Int64(v)
	case let v as Float:		return Int64(v)
	case let v as CGFloat:		return Int64(v)
	case let v as Bool:			return v ? 1 : 0
	case let v as String:		return Int64(v)
	case let v as Substring:	return Int64(v)
	default:return nil
	}
}

public func anyToBool(_ v: Any?) -> Bool? {
	switch v {
	case let v as String:
		return stringToBool(v)
	case let v as Substring:
		return stringToBool(v)
	case let v as Int64:		return v != 0
	case let v as Int:			return v != 0
	case let v as Int8:			return v != 0
	case let v as Int16:		return v != 0
	case let v as Int32:		return v != 0
	case let v as UInt:			return v != 0
	case let v as UInt8:		return v != 0
	case let v as UInt16:		return v != 0
	case let v as UInt32:		return v != 0
	case let v as UInt64:		return v != 0
	case let v as Double:		return v != 0
	case let v as Float:		return v != 0
	case let v as CGFloat:		return v != 0
	case let v as Bool:			return v
	default:return nil
	}
}

public func stringToBool<Argument: StringProtocol>(_ v: Argument) -> Bool {
	guard let ch = v.first else { return false }
	return ["t", "T", "y", "Y", "1"].contains(ch)
}
