import Foundation
import NIO

public final class TCPClient {

	public struct Config {
		public var host: String
		public var port: Int
		public var connectTimeout: TimeDuration
		public var reconnectDelay: TimeDuration

		public init(host: String, port: Int,
					connectTimeout: TimeDuration = .components(seconds: 3),
					reconnectDelay: TimeDuration = .components(seconds: 10)) {
			self.host = host
			self.port = port
			self.connectTimeout = connectTimeout
			self.reconnectDelay = reconnectDelay
		}
	}

	public enum ClientError: Error {
		case notReady
		case cantBind
		case timeout
		case connectionResetByPeer
	}

	public let group: MultiThreadedEventLoopGroup
	public let config: Config
	private var channel: Channel?
	private var messageHandler: MessageHandler?
	
	public init(group: MultiThreadedEventLoopGroup, config: Config) {
		self.group = group
		self.config = config
		channel = nil
		state = .initializing
	}

	deinit {
		assert(.disconnected == state)
	}
	
	public func connect() -> EventLoopFuture<TCPClient> {
		assert(.initializing == state)

		let bootstrap = ClientBootstrap(group: group)
			.channelOption(ChannelOptions.socketOption(.so_reuseaddr), value: 1)
		let handler = MessageHandler(bootstrap: bootstrap, config: self.config)
		_ = bootstrap.channelInitializer { channel in
			channel.pipeline.addHandlers([
				handler,
			])
		}

		state = .connecting("\(config.host):\(config.port)")
		return bootstrap.connect(host: config.host, port: config.port).flatMap { channel in
			self.channel = channel
			self.state = .connected
			self.messageHandler = handler
			return channel.eventLoop.makeSucceededFuture(self)
		}
	}

	public func disconnect() -> EventLoopFuture<Void> {
		guard .connected == state, let channel = channel else {
			return group.next().makeFailedFuture(ClientError.notReady)
		}
		state = .disconnecting
		channel.closeFuture.whenComplete { _ in
			self.state = .disconnected
		}
		channel.close(promise: nil)
		return channel.closeFuture
	}

	public func send(_ data: Data) -> EventLoopFuture<Result<Data, Error>> {
		if .connected != state {
			return group.next().makeFailedFuture(ClientError.notReady)
		}
		guard let channel = channel else {
			return group.next().makeFailedFuture(ClientError.notReady)
		}
		let promise = channel.eventLoop.makePromise(of: Data.self)
		let future = channel.writeAndFlush(ByteBuffer(data: data))
		future.cascadeFailure(to: promise) // if write fails
		return future.flatMap {
			promise.futureResult.map { Result.success($0) }
		}
	}

	private var _state = State.initializing
	private let lock = NSLock()
	private var state: State {
		get {
			lock.withLock { _state }
		}
		set {
			lock.withLock { _state = newValue }
		}
	}

	private enum State: Equatable {
		case initializing
		case connecting(String)
		case connected
		case disconnecting
		case disconnected
	}
	
	private final class Reconnector {
		private var task: RepeatedTask? = nil
		private let bootstrap: ClientBootstrap
		let config: Config

		init(bootstrap: ClientBootstrap, config: Config) {
			self.bootstrap = bootstrap
			self.config = config
		}

		func reconnect(on loop: EventLoop) {
			task = loop.scheduleRepeatedTask(
				initialDelay: .seconds(0),
				delay: .nanoseconds(config.reconnectDelay.nanoseconds)
			) { task in
				print("\(self) reconnecting")
				try self.reconnect()
			}
		}

		private func reconnect() throws {
			bootstrap.connect(host: config.host, port: config.port).whenSuccess { _ in
				print("\(self) reconnect successful!")
				self.task?.cancel()
				self.task = nil
			}
		}
	}

	private final class MessageHandler: ChannelInboundHandler {
		public typealias InboundIn = ByteBuffer
		public typealias OutboundOut = ByteBuffer
		typealias OutboundIn = ByteBuffer

		private var numBytes = 0
		private let reconnector: Reconnector

		init(bootstrap: ClientBootstrap, config: Config) {
			reconnector = Reconnector(bootstrap: bootstrap, config: config)
		}
		
		func channelInactive(context: ChannelHandlerContext) {
			reconnector.reconnect(on: context.eventLoop)
			context.fireChannelInactive()
		}

		func channelRead(context: ChannelHandlerContext, data: NIOAny) {
			var buffer = unwrapInboundIn(data)
			let readableBytes = buffer.readableBytes
			if let message = buffer.readString(length: readableBytes) {
				print(message)
			}
		}
		
		func errorCaught(context: ChannelHandlerContext, error: Error) {
			print("\(self) error: ", error)

			// As we are not really interested getting notified on success or failure we just pass nil as promise to reduce allocations.
			context.close(promise: nil)
		}
	}
}
