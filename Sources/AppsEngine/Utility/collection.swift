//
//  Collection+.swift
//  Spot
//
//  Created by Shawn Clovie on 5/4/16.
//  Copyright © 2016 Shawn Clovie. All rights reserved.
//

import Foundation

extension Dictionary {
	@inlinable
	public func valueForKeys(_ keys: Key...) -> Any? {
		valueForKeys(ArraySlice(keys))
	}
	
	@inlinable
	public func valueForKeys(_ keys: [Key]) -> Any? {
		valueForKeys(ArraySlice(keys))
	}
	
	public func valueForKeys(_ keys: ArraySlice<Key>) -> Any? {
		guard let firstKey = keys.first, let value = self[firstKey] else {
			return nil
		}
		if keys.count == 1 {
			return value
		}
		guard let dict = value as? [Key: Any] else {
			return nil
		}
		return dict.valueForKeys(keys.dropFirst())
	}
}

extension Array {
	/// Safety get element in array at index.
	public func elementAt(_ index: Int) -> Element? {
		indices.contains(index) ? self[index] : nil
	}
}

public final class Ref<T> {
	public var value: T
	
	public init(_ v: T) {
		value = v
	}
}

public let optionalRangeSeparator: Character = "-"

public struct OptionalRange<Bound>: Codable
where Bound: Comparable & Codable & LosslessStringConvertible {
	public static var unbounded: Self {
		Self.init(lower: nil, upper: nil, closed: false)
	}
	
	public var lowerBound: Bound?
	public var upperBound: Bound?
	public var closed: Bool
	
	public init(lower: Bound?, upper: Bound?, closed: Bool) {
		lowerBound = lower
		upperBound = upper
		self.closed = closed
	}
	
	public init(_ range: ClosedRange<Bound>) {
		self.init(lower: range.lowerBound, upper: range.upperBound, closed: true)
	}
	
	public init(_ range: Range<Bound>) {
		self.init(lower: range.lowerBound, upper: range.upperBound, closed: false)
	}
	
	public init(from decoder: Decoder) throws {
		let container = try decoder.singleValueContainer()
		self.init(try container.decode(String.self))
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(description)
	}
	
	public var isEmpty: Bool {
		guard let lower = lowerBound, let upper = upperBound else {
			return false
		}
		return lower == upper
	}
	
	public func contains(_ value: Bound) -> Bool {
		if let lowerBound = lowerBound, value < lowerBound {
			return false
		}
		if let upperBound = upperBound {
			return closed ? value <= upperBound : value < upperBound
		}
		return true
	}
}

extension OptionalRange: JSONCodable {
	public func encodeJSON() throws -> JSON {
		.string(description)
	}
	
	public init(from json: JSON) throws {
		guard case .string(let text) = json else {
			throw Errors.invalidFormat.wrap("\(Self.self) should be string")
		}
		self.init(text)
	}
}

extension OptionalRange: LosslessStringConvertible {
	public init(_ description: String) {
		if description.isEmpty {
			self.init(lower: nil, upper: nil, closed: false)
		} else {
			let comps = description.split(separator: optionalRangeSeparator, maxSplits: 2, omittingEmptySubsequences: false)
			self.init(lower: comps[0].isEmpty ? nil : Bound(String(comps[0])),
					  upper: comps.count < 2 || comps[1].isEmpty ? nil : Bound(String(comps[1])),
					  closed: true)
		}
	}
}

extension OptionalRange: CustomStringConvertible {
	public var description: String {
		if lowerBound == nil && upperBound == nil {
			return ""
		}
		var result = lowerBound?.description ?? ""
		result.append(optionalRangeSeparator)
		if let upper = upperBound {
			result += upper.description
		}
		return result
	}
}
