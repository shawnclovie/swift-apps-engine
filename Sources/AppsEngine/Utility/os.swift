//
//  os.swift
//
//  Created by Shawn Clovie on 21/3/2022.
//

import Foundation

public func observeSignal(_ signals: Int32..., invoke: @escaping (_ sig: Int32) -> Void) {
	guard !signals.isEmpty else { return }
	let signalQueue = DispatchQueue(label: "signal.observer")
	for sig in signals {
		let source = DispatchSource.makeSignalSource(signal: sig, queue: signalQueue)
		source.setEventHandler {
			invoke(sig)
		}
		source.resume()
	}
}
