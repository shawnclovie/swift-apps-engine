import Foundation

public func joinPathComponents(_ comps: String?..., separator: String) -> String {
	let count = comps.count
	if count == 0 {
		return ""
	} else if count == 1 {
		return comps[0] ?? ""
	} else if separator.isEmpty {
		return comps.compactMap { $0 }.joined()
	}
	var lastPath = comps[0] ?? ""
	var buf = ""
	buf.reserveCapacity(128)
	if !lastPath.isEmpty {
		buf.append(lastPath)
	}
	for i in 1..<count {
		guard let comp = comps[i], !comp.isEmpty else {
			continue
		}
		let hasSuffix = lastPath.hasSuffix(separator)
		let hasPrefix = comp.hasPrefix(separator)
		if hasSuffix && hasPrefix {
			buf.append(contentsOf: comp.dropFirst(separator.count))
		} else {
			if !lastPath.isEmpty && !hasSuffix && !hasPrefix {
				buf.append(separator)
			}
			buf.append(comp)
		}
		lastPath = comp
	}
	return buf
}

public func appendPathExtension(_ path: String, ext: String) -> String {
	var buf = path
	if !ext.isEmpty {
		if !ext.hasPrefix(".") {
			buf.append(".")
		}
		buf.append(ext)
	}
	return buf
}
