import Foundation
import AsyncHTTPClient
import NIO
import NIOHTTP1
import Vapor

public enum HTTPConst {
	public static let contentType = "Content-Type"

	public static let contentTypeBinary = "application/octstream"
	public static let contentTypeHTML = "text/html"
	public static let contentTypeJSON = "application/json"
	public static let contentTypeMultipartFormData = "multipart/form-data"
	public static let contentTypeText = "text/plain"
	
	public static let crlf = "\r\n"
}

public typealias HTTPResult = Result<HTTPResponse, WrapError>

public struct HTTPResponse {
	public var status: HTTPResponseStatus
	public var headers: HTTPHeaders
	public var body: ByteBuffer?
	
	public var error: Error?
	public var metricCode: Int?
	public var metricKeySuffix: String?
	
	public init(_ status: HTTPResponseStatus, headers: HTTPHeaders? = nil, body: ByteBuffer? = nil, error: Error? = nil) {
		self.status = status
		self.headers = headers ?? .init()
		self.body = body
		self.error = error
	}
	
	public init(response: HTTPClient.Response) {
		self.init(response.status, headers: response.headers, body: response.body)
	}
	
	public var bytes: ByteBuffer {
		var buf = ByteBuffer()
		buf.writeString(status.code.description)
		buf.writeString(" ")
		buf.writeString(status.reasonPhrase)
		buf.writeString(HTTPConst.crlf)
		for header in headers {
			buf.writeString(header.name)
			buf.writeString(": ")
			buf.writeString(header.value)
			buf.writeString(HTTPConst.crlf)
		}
		buf.writeString(HTTPConst.crlf)
		if var body = body {
			buf.writeBuffer(&body)
		}
		return buf
	}
	
	public static func binary(_ status: HTTPResponseStatus, headers: HTTPHeaders? = nil, _ body: ByteBuffer) -> Self {
		var headers = headers ?? .init()
		headers.replaceOrAdd(name: HTTPConst.contentType, value: HTTPConst.contentTypeBinary)
		return .init(status, headers: headers, body: body)
	}

	public static func error(headers: HTTPHeaders? = nil, _ error: Error) -> Self {
		let err = error as? WrappableError ?? Errors.internal.wrap(error)
		var body: String
		if headers?.contentType ?? .json == .json {
			body = JSON.Encoder().encode([Keys.error: err.encodeJSON()])
		} else {
			body = err.description
			let extra = err.collectExtra()
			if !extra.isEmpty {
				body += "\n"
				body += JSON.object(extra).description
			}
		}
		return .init(err.base.status, headers: headers, body: .init(string: body), error: error)
	}
	
	public static func empty(_ status: HTTPResponseStatus, headers: HTTPHeaders? = nil) -> Self {
		.init(status, headers: headers, body: nil)
	}
	
	public static func text(_ status: HTTPResponseStatus, headers: HTTPHeaders? = nil, _ text: String) -> Self {
		var headers = headers ?? .init()
		headers.replaceOrAdd(name: HTTPConst.contentType, value: HTTPConst.contentTypeText)
		return .init(status, headers: headers, body: ByteBuffer(string: text))
	}

	/// Marshal `serializable` as `Data` with `JSONSerialization`.
	///
	/// Only support primative type for `serializable`, any Swift struct may cause runtime error.
	public static func json(
		_ status: HTTPResponseStatus,
		headers: HTTPHeaders? = nil,
		serializable: Any
	) -> Self {
		do {
			let data = try JSONSerialization.data(withJSONObject: serializable)
			return json(status, data: data)
		} catch {
			return .error(headers: headers, Errors.internal.wrap(error))
		}
	}

	/// Marshal `codable` as `Data` with `JSONEncoder`.
	public static func json<Object>(
		_ status: HTTPResponseStatus,
		headers: HTTPHeaders? = nil,
		_ codable: Object
	) -> Self where Object: Encodable {
		do {
			let data = try JSONEncoder().encode(codable)
			return json(status, data: data)
		} catch {
			return .error(headers: headers, Errors.internal.wrap(error))
		}
	}

	public static func json(
		_ status: HTTPResponseStatus,
		headers: HTTPHeaders? = nil,
		data: Data
	) -> Self {
		var headers = headers ?? .init()
		headers.replaceOrAdd(name: HTTPConst.contentType, value: HTTPConst.contentTypeJSON)
		return .init(status, headers: headers, body: .init(data: data))
	}

	public static func redirect(_ location: String, temporary: Bool) -> Self {
		var inst = Self(temporary ? .temporaryRedirect : .permanentRedirect)
		inst.headers.replaceOrAdd(name: .location, value: location)
		return inst
	}
}
