//
//  json.swift
//  
//
//  Created by Shawn Clovie on 10/6/2022.
//

import Foundation
import NIOCore

public protocol JSONDecodable {
	init(from json: JSON) throws
}

public protocol JSONEncodable {
	func encodeJSON() throws -> JSON
}

public typealias JSONCodable = JSONEncodable & JSONDecodable

public enum JSON {
	public struct Number: Equatable {
		public var sign: FloatingPointSign
		public var units: UInt64
		public var places: UInt32
		
		@inlinable
		public init(sign: FloatingPointSign, uints: UInt64, places: UInt32) {
			self.sign = sign
			self.units = uints
			self.places = places
		}
	}
	
	case null
	case number(Number)
	case string(String)
	case bool(Bool)
	case array([Self])
	case object([String: Self])
}

extension JSON {
	public init(from value: Any?) {
		switch value {
		case let v as Bool:
			self = .bool(v)
		case let v as Int:
			self = .number(.init(integer: .init(v)))
		case let v as Int8:
			self = .number(.init(integer: .init(v)))
		case let v as Int16:
			self = .number(.init(integer: .init(v)))
		case let v as Int32:
			self = .number(.init(integer: .init(v)))
		case let v as Int64:
			self = .number(.init(integer: v))
		case let v as UInt:
			self = .number(.init(unsigned: .init(v)))
		case let v as UInt8:
			self = .number(.init(unsigned: .init(v)))
		case let v as UInt16:
			self = .number(.init(unsigned: .init(v)))
		case let v as UInt32:
			self = .number(.init(unsigned: .init(v)))
		case let v as UInt64:
			self = .number(.init(unsigned: v))
		case let v as Double:
			self = .number(.init(double: v))
		case let v as Float:
			self = .number(.init(double: .init(v)))
		case let v as Float32:
			self = .number(.init(double: .init(v)))
		case let v as Float64:
			self = .number(.init(double: v))
		case let v as String:
			self = .string(v)
		case let v as Substring:
			self = .string(.init(v))
		case let vs as [Any]:
			self = .array(vs.map(Self.init))
		case let vs as [AnyHashable: Any]:
			var obj: [String: Value] = [:]
			obj.reserveCapacity(vs.count)
			for (key, value) in vs {
				obj[key.description] = Self.init(from: value)
			}
			self = .object(obj)
		case let v as JSON:
			self = v
		default:
			self = .null
		}
	}

	public var boolValue: Bool? {
		if case let .bool(v) = self {
			return v
		}
		return nil
	}

	public var numberValue: Number? {
		if case let .number(v) = self {
			return v
		}
		return nil
	}

	public var int64Value: Int64? { numberValue?.as() }

	public var uint64Value: UInt64? { numberValue?.as() }

	public var doubleValue: Double? { numberValue?.as() }

	public var stringValue: String? {
		if case let .string(v) = self {
			return v
		}
		return nil
	}

	public var arrayValue: [JSON]? {
		if case let .array(v) = self {
			return v
		}
		return nil
	}

	public var objectValue: [String: JSON]? {
		if case let .object(v) = self {
			return v
		}
		return nil
	}
	
	public subscript(index: Int) -> Self {
		guard index >= 0, let vs = arrayValue else {
			return .null
		}
		return vs[index]
	}

	public subscript(keys: String...) -> Self {
		self[keys[...]]
	}
	
	public subscript(keys: ArraySlice<String>) -> Self {
		switch self {
		case .number(_):	fallthrough
		case .string(_):	fallthrough
		case .bool(_):		fallthrough
		case .array(_):		fallthrough
		case .null:			return .null
		case .object(let vs):
			guard let key0 = keys.first,
				  let value = vs[key0] else {
				return .null
			}
			return keys.count == 1 ? value : value[keys.dropFirst()]
		}
	}
	
	public struct Decoder {
		public init() {}

		public func decode(_ data: Data) throws -> JSON {
			let decoder = JSONDecoder()
			#if !os(Linux)
			decoder.allowsJSON5 = true
			#endif
			return try decoder.decode(JSON.self, from: data)
		}
	}

	public struct Encoder {
		public var options: JSONEncoder.OutputFormatting
		
		public init(options: JSONEncoder.OutputFormatting = []) {
			self.options = options
		}
		
		public func encode(_ value: JSON) -> String {
			switch value {
			case .null:
				return Const.null.description
			case .number(let v):
				return v.description
			case .string(let v):
				return escape(v)
			case .bool(let v):
				return (v ? Const.true : Const.false).description
			case .array(let vs):
				var buf = ByteBuffer()
				write(value, to: &buf)
				return buf.readString(length: buf.readableBytes, encoding: .utf8)
					?? "[\(vs.map(\.description).joined(separator: ","))]"
			case .object(let vs):
				var buf = ByteBuffer()
				write(value, to: &buf)
				return buf.readString(length: buf.readableBytes, encoding: .utf8)
					?? "{\(vs.map{ "\(escape($0.key)):\($0.value)" }.joined(separator: ","))}"
			}
		}
		
		public func write(_ value: JSON, to buf: inout ByteBuffer, indent: UInt = 0) {
			switch value {
			case .null:
				buf.writeStaticString(Const.null)
			case .number(let v):
				buf.writeString(v.description)
			case .string(let v):
				buf.writeString(escape(v))
			case .bool(let v):
				buf.writeStaticString(v ? Const.true : Const.false)
			case .array(let array):
				write(array: array, into: &buf, indent: indent)
			case .object(let vs):
				write(object: vs, into: &buf, indent: indent)
			}
		}
		
		private func write(array: [JSON], into buf: inout ByteBuffer, indent: UInt) {
			let pretty = options.contains(.prettyPrinted)
			buf.writeStaticString(Const.bracketSquareL)
			var first = true
			for v in array {
				if first {
					first = false
				} else {
					buf.writeStaticString(Const.comma)
				}
				if pretty {
					buf.writeStaticString(Const.lf)
					buf.writeString(Const.indent(count: indent + 1))
				}
				write(v, to: &buf, indent: indent + 1)
			}
			if pretty && !array.isEmpty {
				buf.writeStaticString(Const.lf)
				buf.writeString(Const.indent(count: indent))
			}
			buf.writeStaticString(Const.bracketSquareR)
		}
		
		private func write(object: [String: JSON], into buf: inout ByteBuffer, indent: UInt) {
			buf.writeStaticString(Const.bracketCurlyL)
			if options.contains(.sortedKeys) {
				let keys = Array(object.keys).sorted()
				for (i, k) in keys.enumerated() {
					let v = object[k]!
					if i != 0 {
						buf.writeStaticString(Const.comma)
					}
					write(objectEntry: k, value: v, into: &buf, indent: indent + 1)
				}
			} else {
				var first = true
				for (k, v) in object {
					if first {
						first = false
					} else {
						buf.writeStaticString(Const.comma)
					}
					write(objectEntry: k, value: v, into: &buf, indent: indent + 1)
				}
			}
			if options.contains(.prettyPrinted) && !object.isEmpty {
				buf.writeStaticString(Const.lf)
				buf.writeString(Const.indent(count: indent))
			}
			buf.writeStaticString(Const.bracketCurlyR)
		}
		
		private func write(objectEntry key: String, value: JSON, into buf: inout ByteBuffer, indent: UInt) {
			if options.contains(.prettyPrinted) {
				buf.writeStaticString(Const.lf)
				buf.writeString(Const.indent(count: indent))
			}
			buf.writeString(escape(key))
			buf.writeStaticString(Const.colon)
			if options.contains(.prettyPrinted) {
				buf.writeStaticString(Const.space)
			}
			write(value, to: &buf, indent: indent)
		}
		
		@inline(__always)
		private func escape(_ string: String) -> String {
			Self.escape(string, escapingSlashes: options.contains(.withoutEscapingSlashes))
		}
		
		public static func escape<StringType>(_ string: StringType, escapingSlashes: Bool = true) -> String where StringType: StringProtocol {
			var escaped: [Character] = [Const.quot]
			escaped.reserveCapacity(Int(Float(string.count) * 1.2))
			for ch in string {
				switch ch {
				case "\"":      escaped += "\\\""
				case "\\":      escaped += "\\\\"
				case "/":
					if escapingSlashes {
						escaped += "\\/"
					} else {
						escaped.append(ch)
					}
				case "\u{08}":  escaped += "\\b"
				case "\u{09}":  escaped += "\\t"
				case "\u{0A}":  escaped += "\\n"
				case "\u{0C}":  escaped += "\\f"
				case "\u{0D}":  escaped += "\\r"
				default:        escaped.append(ch)
				}
			}
			escaped.append(Const.quot)
			return String(escaped)
		}
	}

	private enum Const {
		static let null: StaticString = "null"
		static let `true`: StaticString = "true"
		static let `false`: StaticString = "false"
		static let bracketSquareL: StaticString = "["
		static let bracketSquareR: StaticString = "]"
		static let bracketCurlyL: StaticString = "{"
		static let bracketCurlyR: StaticString = "}"
		static let comma: StaticString = ","
		static let colon: StaticString = ":"
		static let space: StaticString = " "
		static let lf: StaticString = "\n"
		static let indent: StaticString = "  "
		
		static let quot: Character = "\""
		
		static func indent(count: UInt) -> String {
			.init(repeating: Const.indent.description, count: Int(count))
		}
	}
}

extension JSON: Equatable {
}

extension JSON: ExpressibleByNilLiteral {
	public init(nilLiteral: ()) {
		self = .null
	}
}

extension JSON: ExpressibleByStringLiteral {
	public init(stringLiteral value: StringLiteralType) {
		self = .string(value)
	}
}

extension JSON: ExpressibleByBooleanLiteral {
	public init(booleanLiteral value: BooleanLiteralType) {
		self = .bool(value)
	}
}

extension JSON: ExpressibleByIntegerLiteral {
	public init(integerLiteral value: IntegerLiteralType) {
		self = .number(value < 0
					   ? .init(integer: Int64(value))
					   : .init(unsigned: UInt64(value)))
	}
}

extension JSON: ExpressibleByFloatLiteral {
	public init(floatLiteral value: FloatLiteralType) {
		self = .number(.init(double: Double(value)))
	}
}

extension JSON: ExpressibleByArrayLiteral {
	public typealias ArrayLiteralElement = JSON
	
	public init(arrayLiteral elements: Value...) {
		self = .array(elements)
	}
}

extension JSON: ExpressibleByDictionaryLiteral {
	public typealias Key = String
	public typealias Value = JSON
	
	public init(dictionaryLiteral elements: (String, Value)...) {
		self = .object([String : Value](elements) { l, r in r })
	}
}

extension JSON: CustomStringConvertible {
	public var description: String {
		Encoder().encode(self)
	}
}

extension JSON: Codable {
	private struct CodingKeys: CodingKey {
		var stringValue: String
		
		init(stringValue: String) {
			self.stringValue = stringValue
		}
		
		var intValue: Int?
		
		init(intValue: Int) {
			self.intValue = intValue
			self.stringValue = ""
		}
	}
	
	public init(from decoder: Swift.Decoder) throws {
		let container = try decoder.singleValueContainer()
		if let v = try? container.decode(Bool.self) {
			self = .bool(v)
		} else if let v = try? container.decode(UInt64.self) {
			self = .number(.init(unsigned: v))
		} else if let v = try? container.decode(Int64.self) {
			self = .number(.init(integer: v))
		} else if let v = try? container.decode(Double.self) {
			self = .number(.init(double: v))
		} else if let v = try? container.decode(String.self) {
			self = .string(v)
		} else if let v = try? container.decode([String: JSON].self) {
			self = .object(v)
		} else if let v = try? container.decode([JSON].self) {
			self = .array(v)
		} else {
			self = .null
		}
	}
	
	public func encode(to encoder: Swift.Encoder) throws {
		switch self {
		case .null:
			var container = encoder.singleValueContainer()
			try container.encodeNil()
		case .number(let v):
			var container = encoder.singleValueContainer()
			try container.encode(v)
		case .bool(let v):
			var container = encoder.singleValueContainer()
			try container.encode(v)
		case .string(let v):
			var container = encoder.singleValueContainer()
			try container.encode(v)
		case .array(let v):
			var container = encoder.singleValueContainer()
			try container.encode(v)
		case .object(let v):
			var container = encoder.container(keyedBy: CodingKeys.self)
			for (key, value) in v {
				try container.encode(value, forKey: CodingKeys(stringValue: key))
			}
		}
	}
}

extension JSON.Number {
	private static let charCode0: UInt8 = 48
	
	public init(integer units: Int64) {
		self.init(sign: units < 0 ? .minus : .plus, uints: UInt64(units), places: 0)
	}
	
	public init(unsigned uints: UInt64) {
		self.init(sign: .plus, uints: uints, places: 0)
	}
	
	public init(double: Double) {
		var str = [Character](abs(double).description)
		var places: UInt32 = 0
		if let dotPos = str.firstIndex(of: ".") {
			places = UInt32(str.count - dotPos - 1)
			str.remove(at: dotPos)
		}
		var units: UInt64 = 0
		for (i, ch) in str.reversed().enumerated() {
			let num = (ch.asciiValue ?? Self.charCode0) - Self.charCode0
			units += UInt64(num) * UInt64(pow(10.0, Float(i)))
		}
		self.init(sign: double.sign, uints: units, places: places)
	}
	
	@inlinable
	public func `as`<Return>() -> Return?
	where Return: FixedWidthInteger & UnsignedInteger {
		guard places == 0 else {
			return nil
		}
		switch sign {
		case .minus:
			return units == 0 ? 0 : nil
		case .plus:
			return Return(exactly: units)
		}
	}
	
	@inlinable
	public func `as`<Return>() -> Return?
	where Return: FixedWidthInteger & SignedInteger {
		guard places == 0 else {
			return nil
		}
		switch sign {
		case .minus:
			let negated = Int64(bitPattern: 0 &- units)
			return negated <= 0 ? Return(exactly: negated) : nil
		case .plus:
			return Return(exactly: units)
		}
	}
	
	@inlinable
	public func `as`<Return>() -> (units: Return, places: Return)?
	where Return: FixedWidthInteger & SignedInteger {
		guard let places = Return(exactly: places) else {
			return nil
		}
		switch sign {
		case .minus:
			let negated = Int64(bitPattern: 0 &- units)
			guard negated <= 0,
				let units = Return(exactly: negated) else {
				return nil
			}
			return (units: units, places: places)
		case .plus:
			guard let units = Return(exactly: self.units) else {
				return nil
			}
			return (units: units, places: places)
		}
	}
	
	@inlinable
	public func `as`<Return>() -> Return
	where Return: BinaryFloatingPoint {
		var places = Int(self.places)
		var units = self.units
		while places > 0 {
			guard case (let quotient, remainder: 0) = units.quotientAndRemainder(dividingBy: 10) else {
				switch sign {
				case .minus:
					return -Return(units) * Self.base10Inverse(x: places, as: Return.self)
				case .plus:
					return  Return(units) * Self.base10Inverse(x: places, as: Return.self)
				}
			}
			units   = quotient
			places -= 1
		}
		switch sign {
		case .minus: return -Return(units)
		case  .plus: return  Return(units)
		}
	}
	
	/// Positive powers of 10, up to [`10_000_000_000_000_000_000`]().
	public static let base10Exp: [UInt64] = [
		1,
		10,
		100,
		
		1_000,
		10_000,
		100_000,
		
		1_000_000,
		10_000_000,
		100_000_000,
		
		1_000_000_000,
		10_000_000_000,
		100_000_000_000,
		
		1_000_000_000_000,
		10_000_000_000_000,
		100_000_000_000_000,
		
		1_000_000_000_000_000,
		10_000_000_000_000_000,
		100_000_000_000_000_000,
		
		1_000_000_000_000_000_000,
		10_000_000_000_000_000_000,
		//  UInt64.max:
		//  18_446_744_073_709_551_615
	]
	
	/// Returns the inverse of the given power of 10.
	@inlinable
	public static func base10Inverse<Number>(x: Int, as _: Number.Type) -> Number
		where Number: BinaryFloatingPoint {
		let inverses: [Number] = [
			1,
			1e-1,
			1e-2,
			
			1e-3,
			1e-4,
			1e-5,
			
			1e-6,
			1e-7,
			1e-8,
			
			1e-9,
			1e-10,
			1e-11,
			
			1e-12,
			1e-13,
			1e-14,
			
			1e-15,
			1e-16,
			1e-17,
			
			1e-18,
			1e-19,
		]
		return inverses[x]
	}
}

extension JSON.Number: CustomStringConvertible {
	public var description: String {
		if self.places == 0 {
			switch sign {
			case .plus:
				return "\(units)"
			case .minus:
				return "-\(units)"
			}
		}
		let places = Int(self.places)
		let unpadded = String(units)
		let zeroCount = Swift.max(0, 1 + places - unpadded.count)
		let string = "\(String.init(repeating: "0", count: zeroCount))\(unpadded)"
		switch sign {
		case .plus:
			return  "\(string.dropLast(places)).\(string.suffix(places))"
		case .minus:
			return "-\(string.dropLast(places)).\(string.suffix(places))"
		}
	}
}

extension JSON.Number: Codable {
	public init(from decoder: Swift.Decoder) throws {
		let container = try decoder.singleValueContainer()
		if let v = try? container.decode(Bool.self) {
			self = .init(integer: v ? 1 : 0)
		} else if let v = try? container.decode(UInt64.self) {
			self = .init(unsigned: v)
		} else if let v = try? container.decode(Int64.self) {
			self = .init(integer: v)
		} else if let v = try? container.decode(Double.self) {
			self = .init(double: v)
		} else if let v = try? container.decode(String.self) {
			self = .init(double: Double(v) ?? 0)
		} else {
			self = .init(integer: 0)
		}
	}

	public func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		if places == 0 {
			switch sign {
			case .plus:
				try container.encode(units)
			case .minus:
				try container.encode(-Int64(units))
			}
		} else {
			let v = Double(units) / pow(10, Double(places)) * (sign == .plus ? 1 : -1)
			try container.encode(v)
		}
	}
}
