import Foundation
import Dispatch

public extension NSLock {
	func withLock<Return>(_ body: () throws -> Return) rethrows -> Return {
		lock()
		defer {
			unlock()
		}
		return try body()
	}

	func withLockAsync<Return>(_ body: () async throws -> Return) async rethrows -> Return {
		lock()
		defer {
			unlock()
		}
		return try await body()
	}
}

@discardableResult
public func waitAsyncTask<Return>(priority: TaskPriority? = nil, operation: @escaping @Sendable () async throws -> Return) -> Result<Return, Error> {
	let sema = DispatchSemaphore(value: 0)
	let ref = Ref<Result<Return, Error>?>(nil)
	Task<Void, Error>(priority: priority) {
		do {
			ref.value = .success(try await operation())
		} catch {
			ref.value = .failure(error)
		}
		sema.signal()
	}
	sema.wait()
	return ref.value!
}

public struct SynchronizableValue<Value> {
	private var value: Value
	private let lock: DispatchSemaphore
	
	public init(_ v: Value, maxAccessCount: Int = 1) {
		value = v
		lock = .init(value: max(1, maxAccessCount))
	}
	
	public func get() -> Value { value }
	
	public func waitAndGet() -> Value {
		lock.wait()
		let v = value
		lock.signal()
		return v
	}
	
	public mutating func waitAndSet(_ v: Value) {
		lock.wait()
		value = v
		lock.signal()
	}
	
	public mutating func waitAndSet(_ v: Value, timeout: DispatchTime) throws {
		switch lock.wait(timeout: timeout) {
		case .success:
			value = v
			lock.signal()
		case .timedOut:
			throw Errors.timeout
		}
	}
	
	@discardableResult
	public mutating func waitAndSet<R>(with fn: (inout Value)->R) -> R {
		lock.wait()
		let r = fn(&value)
		lock.signal()
		return r
	}
	
	@discardableResult
	public mutating func waitAndSet<R>(with fn: (inout Value)->R, timeout: DispatchTime) throws -> R {
		switch lock.wait(timeout: timeout) {
		case .success:
			let r = fn(&value)
			lock.signal()
			return r
		case .timedOut:
			throw Errors.timeout
		}
	}
}
