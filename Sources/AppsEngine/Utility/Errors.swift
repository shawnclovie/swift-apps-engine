import Foundation
import NIOHTTP1

public struct AnyError<Object: Encodable>: Error, Encodable, CustomStringConvertible, LocalizedError, JSONEncodable {
	public let object: Object
	public let wrapped: Error?
	
	public init(_ obj: Object, wrap: Error? = nil) {
		object = obj
		wrapped = wrap
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(description)
	}
	
	public var description: String {
		var desc = "\(object)"
		if let wrapped = wrapped {
			desc.append(extractDescription(wrapped, withCaller: false))
		}
		return desc
	}

	public var errorDescription: String { description }
	
	public func encodeJSON() throws -> JSON {
		.string(description)
	}
}

public protocol WrappableError: Error, CustomStringConvertible {
	/// Get base type
	var base: Errors {get}
	/// Make new error with new type and other original error info
	func rebase(_ base: Errors) -> Self
	/// Get original error
	func unwrap() -> Error?
	
	func collectExtra() -> Errors.Extra
	
	func encodeJSON() -> JSON
}

extension WrappableError {
	public func convertOrWrap(_ err: Error, callerSkip: UInt = 0) -> WrapError {
		if let err = err as? WrapError {
			return err
		}
		if let err = err as? Errors {
			return WrapError(err, callerSkip: 1 + callerSkip)
		}
		return WrapError(base, original: err, callerSkip: 1 + callerSkip)
	}
}

public struct Errors: WrappableError, Equatable, Encodable, Hashable {
	public typealias Extra = [String: JSON]
		
	public let name: String
	public let status: HTTPResponseStatus
	
	public init(_ name: String, _ status: HTTPResponseStatus) {
		self.name = name
		self.status = status
	}
	
	public func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(description)
	}
	
	public func encodeJSON() -> JSON {
		[Keys.name: .string(name)]
	}
	
	public func hash(into hasher: inout Hasher) {
		hasher.combine(name)
		hasher.combine(status.code)
	}
	
	public func error(extra: Errors.Extra? = nil, maxStack: UInt = 1) -> WrapError {
		WrapError(self, extra: extra, callerSkip: 1, maxStack: maxStack)
	}

	public var description: String { name }

	public var localizedDescription: String { description }

	public var base: Errors { self }

	public func rebase(_ base: Errors) -> Self { base }

	public func wrap(_ error: Error, extra: Errors.Extra? = nil, maxStack: UInt = 1) -> WrapError {
		WrapError(self, original: error, extra: extra, callerSkip: 1, maxStack: maxStack)
	}

	public func wrap<Description>(_ errorDesc: Description, extra: Errors.Extra? = nil, maxStack: UInt = 1) -> WrapError where Description: Encodable {
		WrapError(self, original: AnyError(errorDesc), extra: extra,
				  callerSkip: 1, maxStack: maxStack)
	}

	public func unwrap() -> Error? { nil }
	
	public func collectExtra() -> Errors.Extra { .init() }
}

public struct WrapError: WrappableError {
	public private(set) var base: Errors
	public let original: Error?
	public let wrapped: Error?
	public let extra: Errors.Extra?
	public let caller: String
	
	public init(_ base: Errors,
				original: Error? = nil,
				wrapped: Error? = nil,
				extra: Errors.Extra? = nil,
				callerSkip: UInt = 0,
				maxStack: UInt = 1) {
		self.base = base
		self.original = original
		self.wrapped = wrapped
		self.extra = extra
		let stack = CallerStack.capture(skip: 1 + callerSkip, max: maxStack)
		caller = stack.description
	}

	public func rebase(_ base: Errors) -> Self {
		var dup = self
		dup.base = base
		return dup
	}
	
	public func wrap(_ error: Error, extra: Errors.Extra? = nil, maxStack: UInt = 1) -> WrapError {
		WrapError(detectBase(error), original: error, wrapped: self, extra: extra,
				  callerSkip: 1, maxStack: maxStack)
	}
	
	public func wrap<Description>(_ errorDesc: Description, extra: Errors.Extra? = nil, maxStack: UInt = 1) -> WrapError where Description: Encodable {
		WrapError(base, original: AnyError(errorDesc), wrapped: self, extra: extra,
				  callerSkip: 1, maxStack: maxStack)
	}
	
	public func unwrap() -> Error? { original }
	
	public func collectExtra() -> Errors.Extra {
		Self.collectExtra(self)
	}
	
	public var descriptionWithCallerStack: String {
		append(base: base.name, withCaller: true)
	}
	
	public var description: String {
		append(base: base.name, withCaller: false)
	}

	public var localizedDescription: String { description }
	
	public func contains(oneOf errs: Errors...) -> Bool {
		contains(oneOf: Set(errs))
	}
	
	public func contains(oneOf errs: Set<Errors>) -> Bool {
		if errs.contains(base) {
			return true
		}
		if let err = original as? WrappableError, errs.contains(err.base) {
			return true
		}
		if let err = wrapped as? WrappableError, errs.contains(err.base) {
			return true
		}
		return false
	}
	
	public func encodeJSON() -> JSON {
		encodeJSON(withCaller: false)
	}

	public func encodeJSON(withCaller: Bool) -> JSON {
		var err = [Keys.name: JSON.string(base.name)]
		if let original = original {
			err[Keys.original] = extractStructuredError(original, withCaller: withCaller)
		}
		if let wrapped = wrapped {
			err[Keys.wrapped] = extractStructuredError(wrapped, withCaller: withCaller)
		}
		if withCaller {
			err[Keys.caller] = .string(caller)
		}
		if let extra = extra {
			err[Keys.extra] = .object(extra)
		}
		return .object(err)
	}

	private func detectBase(_ err: Error) -> Errors {
		switch err {
		case let e as WrapError:
			return e.base
		case let e as Errors:
			return e
		default:
			return base
		}
	}
	
	private func append(base: String = "", withCaller: Bool) -> String {
		var bracketedDetail = ""
		if let original = original {
			bracketedDetail.append(extractDescription(original, withCaller: withCaller))
		}
		if withCaller {
			bracketedDetail.append(" @\(caller)")
		}
		var str = base
		if !bracketedDetail.isEmpty {
			str.append("(")
			str.append(bracketedDetail)
			str.append(")")
		}
		if let wrapped = wrapped {
			str.append(withCaller ? ")\n:" : ":")
			if let be = wrapped as? WrapError {
				str.append(be.append(withCaller: withCaller))
			} else {
				str.append(extractDescription(wrapped, withCaller: withCaller))
			}
		}
		return str
	}

	private static func collectExtra(_ err: Error) -> Errors.Extra {
		guard let e = err as? WrapError else {
			return .init()
		}
		var extra = e.extra ?? [:]
		if let it = e.original {
			for (key, value) in collectExtra(it) {
				extra[key] = value
			}
		}
		if let it = e.wrapped {
			for (key, value) in collectExtra(it) {
				extra[key] = value
			}
		}
		return extra
	}
}

extension WrapError: Encodable {
	public func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(description)
	}
}

private func extractDescription(_ err: Error, withCaller: Bool) -> String {
	switch err {
	case let e as WrapError:
		return withCaller ? e.descriptionWithCallerStack : e.description
	case let e as Errors:
		return e.description
	case let e as LocalizedError:
		return e.errorDescription ?? "\(e)"
	default:
		return "\(err)"
	}
}

private func extractStructuredError(_ err: Error, withCaller: Bool) -> JSON {
	switch err {
	case let e as WrapError:
		return e.encodeJSON(withCaller: withCaller)
	case let e as Errors:
		return e.encodeJSON()
	case let e as LocalizedError:
		return .string(e.errorDescription ?? "\(e)")
	default:
		return .string("\(err)")
	}
}
