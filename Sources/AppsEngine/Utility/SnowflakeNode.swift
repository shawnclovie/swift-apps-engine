import Foundation
import SpotFlake

public final class SnowflakeNode {
	public static let maxNodeIndex: Int64 = 1023
	public static let shared = SnowflakeNode(index: 0)
	
	var node: Flake.Node
	
	public init(index: Int64) {
		node = .init(node: max(0, index % Self.maxNodeIndex))!
	}
	
	public func reset(index: Int64) {
		node = .init(node: max(0, index % Self.maxNodeIndex))!
	}
	
	public func generate() -> Flake.ID {
		node.generate()
	}
}
