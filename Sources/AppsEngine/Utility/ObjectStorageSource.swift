import Foundation
import SpotFlake

public struct CloudSource: Equatable {
	public var region: String?
	public var secretID: String
	public var secretKey: String
	public var endpoint: URL?
	
	public init(region: String?,
				secretID: String,
				secretKey: String,
				endpoint: URL?) {
		self.region = region
		self.secretID = secretID
		self.secretKey = secretKey
		self.endpoint = endpoint
	}
	
	public init(_ config: [String: String]) {
		region = config[Keys.region]
		secretID = config[Keys.secretID] ?? ""
		secretKey = config[Keys.secretKey] ?? ""
		endpoint = config[Keys.endpoint].flatMap(URL.init(string:))
	}
}

public struct ObjectStorageSource: Equatable {
	public var name: String?
	public var cloud: CloudSource
	public var bucket: String
	public var basepath: String?
	
	public init(source: String?,
				cloud: CloudSource,
				bucket: String) {
		self.name = source
		self.cloud = cloud
		let sep: Character = "/"
		let comps = bucket
			.trimmingCharacters(in: .init(charactersIn: .init(sep)))
			.split(separator: sep, maxSplits: 2)
		self.bucket = comps.isEmpty ? "" : String(comps[0])
		basepath = comps.count > 1 ? comps.dropFirst().joined(separator: .init(sep)) : nil
	}
	
	/// Create with URL string.
	/// - Parameter url: `ENDPOINT/BUCKET/BASEPATH?name=SOURCE&region=REGION&secret=SECRET_ID:SECRET_KEY`
	///
	/// - Example:
	///    - AWS: `/my_bucket?name=aws&region=us-east-1&secret=AKIAZZZZZZZZZZZZDMHP:DZMIeyI30ivK3zzZZzzZZ4LITNyAAaaAAzzp1SPK`
	///    - OSS: `http://oss-cn-beijing.aliyuncs.com/my_bucket?secret=LJLFo2lOE:23j32oklsd9032lkdsfoizze`
	public init(urlString: String) throws {
		guard let comps = URLComponents(string: urlString) else {
			throw Errors.invalidParameter.wrap("URL format invalid")
		}
		var config: [String: String] = [:]
		for it in comps.queryItems ?? [] where !it.name.isEmpty {
			guard let value = it.value, !value.isEmpty else { continue }
			config[it.name] = value
		}
		let secrets = config[Keys.secret]?.split(separator: ":", maxSplits: 2) ?? []
		let cloud = CloudSource(
			region: config[Keys.region],
			secretID: secrets.count > 0 ? String(secrets[0]) : "",
			secretKey: secrets.count > 1 ? String(secrets[1]) : "",
			endpoint: comps.host.flatMap { URL(string: "\(comps.scheme ?? "")://\($0)") }
		)
		self.init(source: config[Keys.name],
				  cloud: cloud,
				  bucket: comps.path)
	}
	
	public init(_ config: [String: String]) throws {
		guard let bucket = config[Keys.bucket] else {
			throw Errors.invalidParameter.wrap("\(Keys.source) or \(Keys.bucket) empty")
		}
		self.init(source: config[Keys.source],
				  cloud: .init(config),
				  bucket: bucket)
	}
	
	public var urlString: String {
		var url = cloud.endpoint.map { $0.absoluteString } ?? ""
		url.append("/\(bucket)")
		if let basepath = basepath, !basepath.isEmpty {
			url.append("/\(basepath)")
		}
		var query: [URLQueryItem] = []
		if let name = name {
			query.append(.init(name: Keys.name, value: name))
		}
		if let region = cloud.region {
			query.append(.init(name: Keys.region, value: region))
		}
		if !cloud.secretID.isEmpty {
			query.append(.init(name: Keys.secret, value: "\(cloud.secretID):\(cloud.secretKey)"))
		}
		if !query.isEmpty {
			url.append("?")
			url.append(query.map { "\($0.name)=\($0.value ?? "")" }.joined(separator: "&"))
		}
		return url
	}
	
	public func fullpath(_ path: String, separator: String = "/") -> String {
		if let basepath = basepath {
			return joinPathComponents(basepath, path, separator: separator)
		}
		return path
	}
}

/// Access control list
public enum ObjectStorageACL: String {
	/// Owner gets `FULL_CONTROL`. The AuthenticatedUsers group gets `READ` access.
	case authenticatedRead = "authenticated-read"

	/// Owner gets `FULL_CONTROL`. Amazon EC2 gets `READ` access to GET an Amazon Machine Image (AMI) bundle from Amazon S3.
	case awsExecRead = "aws-exec-read"

	/// Both the object owner and the bucket owner get `FULL_CONTROL` over the object.
	///
	/// If you specify this canned ACL when creating a bucket, Amazon S3 ignores it.
	case bucketOwnerFullControl = "bucket-owner-full-control"

	/// Object owner gets `FULL_CONTROL`. Bucket owner gets `READ` access.
	///
	/// If you specify this canned ACL when creating a bucket, Amazon S3 ignores it.
	case bucketOwnerRead = "bucket-owner-read"

	/// Owner gets `FULL_CONTROL`. No one else has access rights (default).
	case `private` = "private"

	/// Owner gets `FULL_CONTROL`. The AllUsers group gets `READ` access.
	case publicRead = "public-read"

	/// Owner gets `FULL_CONTROL`. The AllUsers group gets `READ` and `WRITE` access.
	///
	/// Granting this on a bucket is generally not recommended.
	case publicReadWrite = "public-read-write"
}

public struct ObjectStorageFile {
	public var key: String
	public var content: Data?
	public var eTag: String?
	public var lastModified: Time?
	public var contentLength: Int64?
	public var contentType: String?
	public var acl: ObjectStorageACL?

	public init(key: String,
				content: Data? = nil,
				eTag: String? = nil,
				lastModified: Time? = nil,
				contentLength: Int64? = nil,
				contentType: String? = nil,
				acl: ObjectStorageACL? = nil) {
		self.key = key
		self.content = content
		self.eTag = eTag
		self.lastModified = lastModified
		self.contentLength = contentLength
		self.contentType = contentType
		self.acl = acl
	}
}

public protocol ObjectStorageProvider {
	var source: ObjectStorageSource { get }
	func list(prefix: String, continueToken: String?, maxCount: Int) async throws -> ([ObjectStorageFile], String?)
	func head(key: String) async throws -> ObjectStorageFile
	func get(key: String) async throws -> ObjectStorageFile
	func put(key: String, content: Data, contentType: String,
			 expires: Date?, metadata: [String : String]?,
			 acl: ObjectStorageACL) async throws -> ObjectStorageFile
	func delete(keys: [String]) async throws -> [WrapError]
	func signedURL(key: String, duration: TimeDuration) async throws -> URL
}

extension ObjectStorageProvider {
	public func signedURLExistOnly(key: String, duration: TimeDuration) async throws -> URL {
		_ = try await head(key: key)
		return try await signedURL(key: key, duration: duration)
	}
}

public struct ObjectStorageFileSystemProvider: ObjectStorageProvider {
	public static let name = "file_system"
	
	public let source: ObjectStorageSource
	let basepath: URL
	
	public init(source: ObjectStorageSource, basepath: URL) throws {
		self.basepath = basepath
		guard self.basepath.hasDirectoryPath else {
			throw Errors.invalidParameter.wrap("path(\(basepath) should be directory")
		}
		self.source = source
	}
	
	func wrapSyncResult<Return>(_ fn: () throws -> Return, completion: @escaping (Result<Return, WrapError>)->Void) {
		let result: Result<Return, WrapError>
		do {
			result = .success(try fn())
		} catch {
			result = .failure(Errors.io.wrap(error))
		}
		DispatchQueue.main.async {
			completion(result)
		}
	}

	public func list(prefix: String, continueToken: String?, maxCount: Int) async throws -> ([ObjectStorageFile], String?) {
		let path = basepath.appendingPathComponent(prefix)
		let files = try FileManager.default.contentsOfDirectory(atPath: path.path)
		var items: [ObjectStorageFile] = []
		items.reserveCapacity(files.count)
		for file in files {
			items.append(try await head(key: "\(prefix)\(pathSeparator)\(file)"))
		}
		return (items, nil)
	}
	
	public func head(key: String) async throws -> ObjectStorageFile {
		let path = basepath.appendingPathComponent(key)
		let attrs = try FileManager.default.attributesOfItem(atPath: path.path)
		return .init(
			key: key,
			lastModified: (attrs[.modificationDate] as? Date).map({ Time.init($0) }),
			contentLength: anyToInt64(attrs[.size]))
	}
	
	public func get(key: String) async throws -> ObjectStorageFile {
		let path = basepath.appendingPathComponent(key)
		var file = try await head(key: key)
		file.content = FileManager.default.contents(atPath: path.path)
		return file
	}
	
	public func put(key: String, content: Data, contentType: String,
					expires: Date?, metadata: [String : String]?,
					acl: ObjectStorageACL) async throws -> ObjectStorageFile {
		let path = basepath.appendingPathComponent(key)
		try content.write(to: path)
		return .init(key: key, content: content, lastModified: .utc, contentLength: Int64(content.count), acl: acl)
	}

	public func delete(keys: [String]) -> [WrapError] {
		keys.compactMap { key in
			let path = basepath.appendingPathComponent(key)
			do {
				if FileManager.default.fileExists(atPath: path.path) {
					try FileManager.default.removeItem(at: path)
				}
			} catch {
				return Errors.io.wrap(error)
			}
			return nil
		}
	}
	
	public func signedURL(key: String, duration: TimeDuration) async throws -> URL {
		basepath.appendingPathComponent(key)
	}
}
