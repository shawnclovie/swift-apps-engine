import Foundation
import NIOPosix
import struct NIO.TimeAmount
import SpotFlake

public typealias Time = SpotFlake.Time
public typealias TimeDuration = SpotFlake.TimeDuration

/// An internal helper that formats dates as RFC3339
public final class RFC3339 {
	public static let dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
	static let offsetPositionToMut = [
		1: 36000,
		2: 3600,
		4: 60,
		5: 1,
	]

	/// Thread-specific RFC3339
	private static let thread: ThreadSpecificVariable<RFC3339> = .init()
	
	/// A static RFC1123 helper instance
	public static var shared: RFC3339 {
		if let existing = thread.currentValue {
			return existing
		} else {
			let new = RFC3339()
			thread.currentValue = new
			return new
		}
	}
	
	/// The RFC3339 formatter
	private var formatters: [Int: DateFormatter] = [:]
	private let formatterUTC = DateFormatter()
	
	/// Creates a new RFC3339 helper
	private init() {
		formatterUTC.locale = Locale(identifier: "en_US_POSIX")
		formatterUTC.timeZone = .init(secondsFromGMT: 0)
		formatterUTC.dateFormat = Self.dateFormat
		formatterUTC.calendar = Calendar(identifier: .iso8601)
	}
	
	private func formatter(for offset: Int) -> DateFormatter {
		if let formatter = offset == 0 ? formatterUTC : formatters[offset] {
			return formatter
		}
		let formatter = DateFormatter()
		formatter.locale = formatterUTC.locale
		formatter.timeZone = .init(secondsFromGMT: offset)!
		formatter.dateFormat = Self.dateFormat
		formatter.calendar = Calendar(identifier: .iso8601)
		formatters[offset] = formatter
		return formatter
	}
	
	public func string(from date: Date, offset: Int) -> String {
		formatter(for: offset).string(from: date)
	}
	
	public func date(from string: String) -> Date? {
		formatterUTC.date(from: string)
	}
}

extension Time {
	public func isSameDay(_ other: Time) -> Bool {
		let d1 = date
		let d2 = other.date
		return d1.year == d2.year && d1.month == d2.month && d1.day == d2.day
	}

	public init?(rfc3339 string: String) {
		guard let date = RFC3339.shared.date(from: string) else {
			return nil
		}
		var offset = 0
		if let posPlus = string.lastIndex(of: "+") {
			let compOffset = string[posPlus...]
			for (index, ch) in compOffset.enumerated() {
				guard let v = ch.wholeNumberValue, v > 0,
					  let mut = RFC3339.offsetPositionToMut[index]
				else { continue }
				offset += v * mut
			}
		}
		self.init(date, offset: offset)
	}

	public var rfc3339String: String {
		RFC3339.shared.string(from: asDate, offset: offset)
	}
}

extension Time: Codable {
	public init(from decoder: Decoder) throws {
		let text = try decoder.singleValueContainer().decode(String.self)
		guard let date = RFC3339.shared.date(from: text) else {
			throw Errors.invalidFormat.wrap(text)
		}
		self.init(date)
	}

	public func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(rfc3339String)
	}
}

extension Time: JSONCodable {
	public init(from json: JSON) throws {
		guard case .string(let text) = json else {
			throw Errors.invalidFormat.wrap("\(Self.self) should be RFC3339 string")
		}
		guard let date = RFC3339.shared.date(from: text) else {
			throw Errors.invalidFormat.wrap(text)
		}
		self.init(date)
	}
	
	public func encodeJSON() throws -> JSON {
		.string(rfc3339String)
	}
}

extension TimeDuration {
	public var amount: TimeAmount {
		.nanoseconds(nanoseconds)
	}
}
