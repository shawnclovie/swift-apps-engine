import Foundation

public enum Keys {
	public static let appID = "app_id"
	public static let allowCredentials = "allow_credentials"
	public static let allowedHeaders = "allowed_headers"
	public static let allowedMethods = "allowed_methods"
	public static let allowedOrigin = "allowed_origin"
	public static let bucket = "bucket"
	public static let cacheExpiration = "cache_expiration"
	public static let caller = "caller"
	public static let corsOptions = "cors_options"
	public static let createTime = "create_time"
	public static let `default` = "default"
	public static let enabled = "enabled"
	public static let endpoint = "endpoint"
	public static let error = "error"
	public static let exposedHeaders = "exposed_headers"
	public static let extra = "extra"
	public static let extraAllowedHeaders = "extra_allowed_headers"
	public static let host = "host"
	public static let id = "id"
	public static let name = "name"
	public static let original = "original"
	public static let port = "port"
	public static let secret = "secret"
	public static let secretID = "secret_id"
	public static let secretKey = "secret_key"
	public static let source = "source"
	public static let timeOffset = "time_offset"
	public static let traceID = "trace_id"
	public static let traceDuration = "trace_dur"
	public static let updateTime = "update_time"
	public static let url = "url"
	public static let userID = "user_id"
	public static let region = "region"
	public static let wrapped = "wrapped"

	public static let envRuntimeVerbose = "RUNTIME_VERBOSE"
}

extension Errors {
	public static let apiRateLimit = Errors("api_rate_limit", .tooManyRequests)
	public static let forbidden = Errors("forbidden", .forbidden)
	public static let io = Errors("io", .internalServerError)
	public static let `internal` = Errors("internal", .internalServerError)
	public static let invalidFormat = Errors("invalid_format", .badRequest)
	public static let invalidParameter = Errors("invalid_parameter", .badRequest)
	public static let invalidFormData = Errors("invalid_form_data", .badRequest)
	public static let notFound = Errors("not_found", .notFound)
	public static let timeout = Errors("timeout", .requestTimeout)
	public static let uncaught = Errors("uncaught", .internalServerError)

	public static let noBody = invalidParameter.wrap("no_body")
	public static let noContentType = invalidParameter.wrap("no_content_type")

	public static let appIDEmpty = Errors("app_id_empty", .badRequest)
	public static let userIDEmpty = Errors("user_id_empty", .badRequest)
	public static let appConfigInvalid = Errors("app_config_invalid", .expectationFailed)
	public static let appDownloadFailed = Errors("app_download_failed", .serviceUnavailable)
	public static let appNotChanged = Errors("app_not_changed", .notModified)
	public static let appTestFailed = Errors("app_test_failed", .expectationFailed)
	public static let authFailed = Errors("auth_failed", .unauthorized)
	public static let backendUnavailable = Errors("backend_unavailable", .badRequest)
	public static let endpointNotFound = Errors("endpoint_not_found", .badRequest)
	public static let gateway = Errors("gateway", .badGateway)
	public static let engineAppNotFound = Errors("engine_app_not_found", .badRequest)
	public static let routeNotFound = Errors("route_not_found", .badRequest)
	public static let serverConfigInvalid = Errors("server_config_invalid", .internalServerError)
	public static let signatureInvalid = Errors("sig_invalid", .badRequest)

	public static let database = Errors("database", .internalServerError)
	public static let databaseConstraintViolation = Errors("db_constraint_violation", .badRequest)
	public static let databaseNoChanges = Errors("db_no_changes", .badRequest)
	public static let redis = Errors("redis", .internalServerError)
}
