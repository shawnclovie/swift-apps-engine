import Foundation

public protocol Module {
	var moduleName: String { get }

	var endpoints: [Endpoint] { get }

	var isSegmental: Bool { get }

	func parseConfig(_ configSet: AppConfigSet, segmentID: String) -> AppConfigParseResult?
}

public extension Module {
	var isSegmental: Bool { false }

	func parseConfig(_: AppConfigSet, segmentID _: String) -> AppConfigParseResult? {
		nil
	}
}

public struct AppConfigParseResult {
	public var object: Any?
	public var warnings: [String]

	public init(_ obj: Any? = nil, warnings: [String] = []) {
		object = obj
		self.warnings = warnings
	}
}

public struct AppConfigSet {
	public let rootPath: URL
	public var core: AppConfig
	public let segment: AppSegmentConfig?
	var segmentals: [String: AppConfig] = [:]

	init(rootPath: URL) throws {
		self.rootPath = rootPath
		core = try .init(rootPath: rootPath)
		segment = try .init(appPath: rootPath)
	}

	public var metricName: String { core.metricName }

	/// - Returns: config issue as warning - `{segmentID: {module: warning_details}}`
	mutating func load(for modules: [Module]) -> [String: [String: [String]]] {
		var warnings: [String: [String: [String]]] = [:]
		var config = core
		config.segmentID = segment?.defaultID ?? ""
		parse(with: modules, for: &config).map {
			warnings[config.segmentID] = $0
		}
		core = config
		if let segment = segment {
			let segmentalMods = modules.filter { $0.isSegmental }
			for seg in segment.segments {
				guard seg.id != core.segmentID else { continue }
				var config = core
				config.segmentID = seg.id
				parse(with: segmentalMods, for: &config).map {
					warnings[config.segmentID] = $0
				}
				segmentals[config.segmentID] = config
			}
		}
		return warnings
	}

	private mutating func parse(with mods: [Module], for config: inout AppConfig) -> [String: [String]]? {
		var warnings: [String: [String]] = [:]
		for mod in mods {
			guard let res = mod.parseConfig(self, segmentID: config.segmentID) else {
				continue
			}
			if !res.warnings.isEmpty {
				warnings[mod.moduleName] = res.warnings
			}
			if let v = res.object {
				config.parsed[ObjectIdentifier(type(of: v))] = v
			}
		}
		return warnings.isEmpty ? nil : warnings
	}
}

public struct AppConfig {
	public static let mainFile = "config.json"
	public static let segmentFile = "segment.json"

	public let appID: String
	public let appName: String
	public private(set) var hosts: Set<String> = []
	public private(set) var timeOffset = 0

	public fileprivate(set) var segmentID = ""
	
	public var cors: CORSOptions?

	public let rawData: [String: Any]
	var parsed: [ObjectIdentifier: Any] = [:]

	init(rootPath: URL) throws {
		let content = try Data(contentsOf: rootPath.appendingPathComponent(Self.mainFile))
		let obj = try JSONSerialization.jsonObject(with: content)
		guard let raw = obj as? [String: Any],
		      let appID = raw[Keys.appID] as? String
		else {
			throw Errors.appConfigInvalid.wrap("config should be dictionary with \(Keys.appID) on \(rootPath)")
		}
		rawData = raw
		self.appID = appID
		appName = rawData[Keys.name] as? String ?? ""
		if let hosts = rawData["hosts"] as? [String] {
			for host in hosts {
				self.hosts.insert(host)
			}
		}
		timeOffset = rawData[Keys.timeOffset] as? Int ?? ServerConfig.shared.timezone.offset
		cors = (rawData[Keys.corsOptions] as? [String: Any]).flatMap {
			guard anyToBool($0[Keys.enabled]) == true else {
				return nil
			}
			return CORSOptions.init(from: $0)
		}
	}

	public var metricName: String {
		appName.isEmpty ? appID : appName
	}

	public func get<As>(_ configType: As.Type) -> As? {
		parsed[ObjectIdentifier(configType)] as? As
	}
}

public struct AppSegmentConfig {
	let segments: [Segment]
	let defaultID: String

	init?(appPath: URL) throws {
		let path = appPath.appendingPathComponent(AppConfig.segmentFile)
		guard FileManager.default.fileExists(atPath: path.path) else {
			return nil
		}
		let content = try Data(contentsOf: path)
		let obj = try JSONSerialization.jsonObject(with: content)
		guard let raw = obj as? [String: Any] else {
			throw Errors.appConfigInvalid.wrap("segment config should be dictionary")
		}
		defaultID = raw["default_segment_id"] as? String ?? ""
		let rawSegs = raw["segments"] as? [[String: Any]] ?? []
		segments = rawSegs.map(Segment.init(rawData:))
	}

	public struct Segment {
		public let id: String
		public let name: String

		init(rawData: [String: Any]) {
			id = rawData[Keys.id] as? String ?? ""
			name = rawData[Keys.name] as? String ?? ""
		}
	}
}
