import Foundation
import SWCompression

public protocol AppConfigUpdateListener: AnyObject {
	func updateDidFinish(updatedAppIDs: Set<String>, removedAppIDs: Set<String>) -> Void
}

public protocol AppConfigUpdator {
	func update(input: AppConfigUpdateInput) async throws -> AppConfigUpdateResult
}

public struct AppConfigUpdateInput {
	public let appSource: ServerConfig.AppSource
	public let rootPath: URL
	public let logger: Logger

	public let includesAppIDs: Set<String>
	public let appUpdateTimes: [String: Time]
	public let endpoints: Endpoint.Register
	public let modules: [Module]
	public let skipIfNoChange: Bool
	
	public func includes(appID: String) -> Bool {
		includesAppIDs.isEmpty || includesAppIDs.contains(appID)
	}
	
	public func shouldUpdate(appID: String, updateTime: Time) -> Bool {
		if !skipIfNoChange, let current = appUpdateTimes[appID] {
			return updateTime > current
		}
		return true
	}
	
	public func testApp(directory: URL) -> WrapError? {
		do {
			_ = try EngineApp(
				path: directory, predefinedEndpoints: endpoints,
				modules: modules,
				requestProcessor: nil, apiMetric: nil)
			return nil
		} catch {
			return Errors.appConfigInvalid.convertOrWrap(error)
		}
	}
}

public struct AppConfigUpdateResult {
	public var appFilesStatus: [String: Time] = [:]
	public var updatedApps: [String: Time] = [:]
	public var skippedApps: [String: WrapError] = [:]
	
	public mutating func skipSinceNotChanged(appID: String) {
		skippedApps[appID] = Errors.appNotChanged.wrap(appID)
	}

	public mutating func skip(appID: String, since: WrapError) {
		skippedApps[appID] = since
	}
}

public final class AppConfigProvider {
	public typealias UpdatorBuilder = (_ source: ObjectStorageProvider) -> AppConfigUpdator

	static let cacheDir = "data_cache"
	
	let appSource: ServerConfig.AppSource
	var endpoints = Endpoint.Register()
	let modules: [Module]
	var appUpdateTimes: [String: Time] = [:]
	let updator: AppConfigUpdator

	private let updateQueue = DispatchQueue(label: "update_apps")
	private var isUpdating = false
	private var updateHandlers: [AppConfigUpdateListener] = []

	init(modules: [Module],
		 appSource: ServerConfig.AppSource,
		 appUpdaterBuilder: AppConfigProvider.UpdatorBuilder?) throws {
		updator = { () -> AppConfigUpdator? in
			guard let storage = appSource.objectStorage else { return nil }
			guard let builder = appUpdaterBuilder else {
				Logger.startup.log(.warn, "\(Self.self) no builder passed-in", .init("storage", storage))
				return nil
			}
			guard let provider = Resource.shared.defaultGroup?.objectStorages[storage] else {
				Logger.startup.log(.warn, "\(Self.self) no provider from config", .init("storage", storage))
				return nil
			}
			return builder(provider)
		}() ?? LocalAppConfigUpdator()
		self.appSource = appSource
		self.modules = modules
		for mod in modules {
			try endpoints.register(mod.endpoints)
		}
	}
	
	public var appIDs: [String] { .init(appUpdateTimes.keys) }

	public var isLocal: Bool { updator is LocalAppConfigUpdator }

	public var rootPath: URL {
		isLocal
			? appSource.localAppsPath
			: URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
				.appendingPathComponent(Self.cacheDir)
	}

	public func register(updateHandler: AppConfigUpdateListener) {
		updateHandlers.append(updateHandler)
	}
	
	public func unregister(updateHandler: AppConfigUpdateListener) {
		if let index = updateHandlers.firstIndex(where: { $0 === updateHandler }) {
			updateHandlers.remove(at: index)
		}
	}
	
	func startUpdate() async throws {
		_ = try await self.updateApps(.startup, updator: self.updator)
		if appSource.pullInterval.seconds > 0 {
			scheduleNextUpdate(updator: updator)
		}
	}
	
	func scheduleNextUpdate(updator: AppConfigUpdator) {
		let interval = Int(appSource.pullInterval.seconds)
		updateQueue.asyncAfter(deadline: .now() + .seconds(interval)) {
			Task(priority: .background) {
				let logger = Logger.default
				do {
					_ = try await self.updateApps(logger, updator: updator)
				} catch {
					logger.log(.error, "\(self) update failed", .error(error))
				}
			}
			self.scheduleNextUpdate(updator: updator)
		}
	}
	
	private func updateApps(
		_ logger: Logger,
		updator: AppConfigUpdator,
		includesAppIDs: Set<String> = [],
		skipIfNoChange: Bool = true) async throws -> AppConfigUpdateResult
	{
		isUpdating = true
		defer {
			isUpdating = false
		}
		logger.log(.info, "\(type(of: self)) update: start", .init("rootPath", rootPath))
		let result = try await updator.update(input: .init(
			appSource: appSource,
			rootPath: rootPath,
			logger: logger,
			includesAppIDs: includesAppIDs,
			appUpdateTimes: appUpdateTimes,
			endpoints: endpoints,
			modules: modules,
			skipIfNoChange: skipIfNoChange))
		let removedAppIDs = appUpdateTimes.keys.filter({ result.updatedApps[$0] == nil })
		if !result.updatedApps.isEmpty {
			for (appID, updateTime) in result.updatedApps {
				appUpdateTimes[appID] = updateTime
			}
		}
		for appID in removedAppIDs {
			appUpdateTimes.removeValue(forKey: appID)
		}
		
		notifyUpdateDidFinish(updatedAppIDs: .init(result.updatedApps.keys), removedAppIDs: Set(removedAppIDs), sync: false)

		logger.log(.info, "\(type(of: self)) update: finished", .init("apps", appIDs.joined(separator: ",")))
		if !result.skippedApps.isEmpty {
			logger.log(.warn, "\(type(of: self)) update: skipped some apps", .init("apps", result.skippedApps))
		}
		return result
	}
	
	private func notifyUpdateDidFinish(updatedAppIDs: Set<String>, removedAppIDs: Set<String>, sync: Bool) {
		guard !updatedAppIDs.isEmpty || !removedAppIDs.isEmpty else {
			return
		}
		for hnd in updateHandlers {
			let fn = {
				hnd.updateDidFinish(updatedAppIDs: updatedAppIDs, removedAppIDs: removedAppIDs)
			}
			if sync {
				fn()
			} else {
				Task(priority: .background) {
					fn()
				}
			}
		}
	}
}

public struct LocalAppConfigUpdator: AppConfigUpdator {
	public func update(input: AppConfigUpdateInput) async throws -> AppConfigUpdateResult {
		input.logger.log(.debug, "start reload apps from local", .init("root_path", input.rootPath))
		var result = AppConfigUpdateResult()
		let resKeys: Set<URLResourceKey> = [.isDirectoryKey, .contentModificationDateKey]
		let files = try FileManager.default.contentsOfDirectory(at: input.rootPath, includingPropertiesForKeys: .init(resKeys))
		for file in files {
			let stat = try file.resourceValues(forKeys: resKeys)
			guard stat.isDirectory == true else { continue }
			let appID = file.lastPathComponent
			guard input.includes(appID: appID),
				  let modifyTime = stat.contentModificationDate.map({ Time($0) })
			else { continue }
			guard input.shouldUpdate(appID: appID, updateTime: modifyTime) else {
				result.skipSinceNotChanged(appID: appID)
				continue
			}
			if let error = input.testApp(directory: file) {
				result.skip(appID: appID, since: error)
				continue
			}
			result.updatedApps[appID] = modifyTime
		}
		return result
	}
}

public class ZippedAppConfigUpdater: AppConfigUpdator {
	public static let appConfigExt = ".zip"
	public static let updateTMPDir = "update_tmp"

	public let provider: ObjectStorageProvider

	public init(provider: ObjectStorageProvider) {
		self.provider = provider
	}

	public func update(input: AppConfigUpdateInput) async throws -> AppConfigUpdateResult {
		let curDir = FileManager.default.currentDirectoryPath
		let updateTMPDir = URL(fileURLWithPath: curDir)
			.appendingPathComponent(Self.updateTMPDir)
		try Self.prepare(directory: input.rootPath)
		try Self.prepare(directory: updateTMPDir)

		input.logger.log(.debug, "start download apps", .init("cur_dir", curDir))
		let providerPath = input.appSource.path ?? ""
		let keyPrefix = providerPath +
			(providerPath.isEmpty || providerPath.hasSuffix("/") ? "" : "/")
		var result = AppConfigUpdateResult()
		result.appFilesStatus = try await downloadAppList(provider: provider, keyPrefix: keyPrefix)
		try removeDeletedAppsDirectories(incomingAppIDs: .init(result.appFilesStatus.keys), on: input.rootPath)
		for (appID, lastModified) in result.appFilesStatus {
			guard input.includes(appID: appID) else { continue }
			guard input.shouldUpdate(appID: appID, updateTime: lastModified)
			else {
				result.skipSinceNotChanged(appID: appID)
				continue
			}
			let tmpAppDir = updateTMPDir.appendingPathComponent(appID)
			let file: ObjectStorageFile
			do {
				let key = "\(keyPrefix)\(appID)\(Self.appConfigExt)"
				file = try await downloadZippedAppData(provider: provider, key: key, appDir: tmpAppDir)
			} catch {
				result.skip(appID: appID, since: Errors.appDownloadFailed.convertOrWrap(error))
				continue
			}
			// test app valid
			if let error = input.testApp(directory: tmpAppDir) {
				result.skip(appID: appID, since: error)
				continue
			}
			let appDir = input.rootPath.appendingPathComponent(appID)
			if FileManager.default.fileExists(atPath: appDir.path) {
				try FileManager.default.removeItem(at: appDir)
			}
			try FileManager.default.moveItem(at: tmpAppDir, to: appDir)
			result.updatedApps[appID] = file.lastModified ?? .utc
		}
		return result
	}
	
	func downloadAppList(provider: ObjectStorageProvider, keyPrefix: String) async throws -> [String: Time] {
		var nextToken: String?
		var status: [String: Time] = [:]
		repeat {
			let (files, next) = try await provider.list(prefix: keyPrefix, continueToken: nextToken, maxCount: 128)
			for file in files {
				guard let appID = appID(file: file),
					  let modTime = file.lastModified else { continue }
				status[appID] = modTime
			}
			nextToken = next
		} while nextToken != nil
		return status
	}
	
	func appID(file: ObjectStorageFile) -> String? {
		let filename = URL(fileURLWithPath: file.key).lastPathComponent
		guard let range = filename.range(of: Self.appConfigExt, options: [.backwards]) else {
			return nil
		}
		return String(filename[..<range.lowerBound])
	}
	
	func removeDeletedAppsDirectories(incomingAppIDs: Set<String>, on rootPath: URL) throws {
		for file in try FileManager.default.contentsOfDirectory(at: rootPath, includingPropertiesForKeys: nil) {
			if !incomingAppIDs.contains(file.lastPathComponent) {
				try FileManager.default.removeItem(at: file)
			}
		}
	}
	
	func downloadZippedAppData(provider: ObjectStorageProvider, key: String, appDir: URL) async throws -> ObjectStorageFile {
		let file = try await provider.get(key: key)
		guard let content = file.content else {
			throw Errors.notFound.wrap("no_data")
		}
		if FileManager.default.fileExists(atPath: appDir.path) {
			try FileManager.default.removeItem(at: appDir)
		}
		try FileManager.default.createDirectory(at: appDir, withIntermediateDirectories: true)
		let zipFile = appDir.appendingPathComponent("_.zip")
		try content.write(to: zipFile)
		defer {
			try? FileManager.default.removeItem(at: zipFile)
		}
		let entries = try ZipContainer.open(container: content)
		for entry in entries where entry.info.type == .regular {
			guard let data = entry.data else {
				continue
			}
			let path = appDir.appendingPathComponent(entry.info.name)
			let dir = path.deletingLastPathComponent()
			if !FileManager.default.fileExists(atPath: dir.path) {
				try FileManager.default.createDirectory(at: dir, withIntermediateDirectories: true)
			}
			try data.write(to: path)
		}
		return file
	}
	
	static func prepare(directory: URL) throws {
		let fileMGR = FileManager.default
		if fileMGR.fileExists(atPath: directory.path) {
			if try directory.resourceValues(forKeys: [.isDirectoryKey]).isDirectory != true {
				try fileMGR.removeItem(at: directory)
				try fileMGR.createDirectory(at: directory, withIntermediateDirectories: true)
			}
		} else {
			try fileMGR.createDirectory(at: directory, withIntermediateDirectories: true)
		}
	}
}
