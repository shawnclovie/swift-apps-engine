import Foundation
import NIO

public enum Base {
	/// Initilaize UUID (snowflake)
	/// - Parameters:
	///   - nodeIndex: the index would be part of UUID, it should between [0...1023] (10 bits).
	public static func resetUUID(nodeIndex: UInt16) {
		Logger.default.log(.info, "init snowflake node", .init("index", nodeIndex))
		SnowflakeNode.shared.reset(index: Int64(nodeIndex))
	}
	
	/// Product node index with LanIP and PID.
	///
	/// The function may produce duplicate value while multiple deployment.
	public static func produceNodeIndexWithLanIPAndPID() throws -> UInt16 {
		let seedBits: Int64 = 5
		let maxSeed: Int64 = 1 << seedBits

		let ip = try SocketAddress.lanAddress()
		var ipSeed = Int64(127001)
		if let addr = ip.ipAddress,
		   let seed = Int64(addr.replacingOccurrences(of: ".", with: "")) {
			ipSeed = seed
		}
		let pid = ProcessInfo.processInfo.processIdentifier
		return UInt16((ipSeed % maxSeed) << seedBits) + UInt16(Int64(pid) % maxSeed)
	}
}
