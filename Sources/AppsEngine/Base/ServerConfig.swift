import Fluent
import Vapor
import Yams

public enum Env: String, Sendable {
	case product, test, develop
}

extension Logger {
	public internal(set) static var `default` = Logger()
	static var startup = Logger()
}

public struct ServerConfig: CustomStringConvertible {
	public static var shared = ServerConfig()
	
	public private(set) var workingDirectory = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
	
	public var name = "server"

	public private(set) var env: Env = .product

	public private(set) var server = APIServer(host: "0.0.0.0", port: 3000)

	public private(set) var timezone = Zone(name: "GMT", offset: 0)

	public private(set) var appSource: AppSource
	
	private(set) var resource: Resource.Config?

	public private(set) var rawData: [String: Any] = [:]
	
	init() {
		appSource = .init(localAppsPath: workingDirectory.appendingPathComponent("apps"))
	}

	/// - Parameters:
	///   - appUpdatorBuilder: build updator to update apps with `ObjectStorageProvider` if configure, e.g. `ZippedAppConfigUpdater` with `ObjectStorageAWSS3Provider`.
	public mutating func load(
		workingDirectory: URL,
		configFile: String = "config.yaml",
		localAppDirectory: String = "apps") throws {
		self.workingDirectory = workingDirectory

		let configPath = workingDirectory.appendingPathComponent(configFile)
		guard let content = FileManager.default.contents(atPath: configPath.path) else {
			throw AnyError("\(configPath) load failed")
		}
		var decoded: Any?
		switch configPath.pathExtension {
		case "json":
			decoded = try JSONSerialization.jsonObject(with: content)
		case "yaml", "yml":
			decoded = try Yams.load(yaml: String(decoding: content, as: UTF8.self))
		default:
			throw AnyError("unknown format")
		}
		if let dict = decoded as? [String: Any] {
			rawData = dict
		} else {
			throw AnyError("contents of \(configPath) should be dictionary")
		}

		env = Env.init(rawValue: rawData["env"] as? String ?? "") ?? env
		if let v = rawData["server"] as? [String: Any] {
			server.merge(v)
		}
		
		if let v = rawData["timezone"] as? String {
			guard let tz = TimeZone(identifier: v) else {
				throw AnyError("invalid timezone: '\(v)'")
			}
			timezone = .init(name: v, offset: tz.secondsFromGMT())
		}

		var loggers = [String: Logger]()
		try (rawData["logger"] as? [String: Any]).map({
			for it in $0 {
				guard let cfg = it.value as? [String: Any] else { continue }
				let outputs = try parseLogOutputer(appName: name, config: cfg)
				loggers[it.key] = .init(outputers: outputs)
			}
		})
		loggers[Keys.default].map({ Logger.default = $0 })
		if let logger = loggers["startup"] {
			Logger.startup = logger
		}

		do {
			let metric = rawData["metric"] as? [String: Any] ?? [:]
			let host = metric[Keys.host] as? String ?? "127.0.0.1"
			let port = metric[Keys.port] as? Int ?? 8125
			try Metric.shared.setup(host: host, port: port)
		} catch {
			throw AnyError("metric setup failed", wrap: error)
		}

		if let cfg = rawData["resources"] as? [String: Any] {
			resource = try .init(cfg)
		} else {
			throw AnyError("resources should be dictionary")
		}

		appSource.localAppsPath = workingDirectory.appendingPathComponent(localAppDirectory)
		if let cfg = rawData["app_source"] as? [String: Any] {
			appSource.parse(cfg)
		}
	}

	func parseLogOutputer(appName: String, config: [String: Any]) throws -> [LogOutputer] {
		var outputers: [LogOutputer] = []
		for it in config {
			guard let vs = it.value as? [AnyHashable: Any] else {
				throw AnyError("'log.\(it.key)' should be map")
			}
			let level = Log.Level(rawValue: vs["level"] as? String ?? "") ?? .info
			let output: LogOutputer
			switch it.key {
			case "console":
				output = LogConsoleOutputer(level: level, stream: .init(rawValue: vs["stream"] as? String ?? "") ?? .stdout)
			// TODO: file logger
//			case "file":
//				output = parseFileLogger(name, format, level, vs)
			case "tcp":
				guard let port = vs[Keys.port] as? Int, port > 0 else {
					throw AnyError("'log.\(it.key)'.port should > 0")
				}
				output = LogTCPOutputer(
					level: level,
					options: .init(host: vs[Keys.host] as? String ?? "127.0.0.1", port: port))
			default:
				throw AnyError("'log.\(it.key)' unknown output type")
			}
			outputers.append(output)
		}
		return outputers
	}
	
	public var description: String {
		let desc = [
			"env=\(env)",
			"server=\(server)",
			"timezone=\(timezone)",
			"app_source=\(appSource)",
			"resource=\(resource as Any)"
		]
		return desc.joined(separator: "\n")
	}
	
	public struct APIServer: Sendable {
		public internal(set) var host: String
		public internal(set) var port: Int
		
		mutating func merge(_ v: [String: Any]) {
			(v[Keys.host] as? String).map({host = $0})
			(v[Keys.port] as? Int).map({port = $0})
		}
	}
	
	public struct AppSource {
		public var pullInterval: TimeDuration = .zero
		public var objectStorage: String?
		public var path: String?
		public var localAppsPath: URL
		
		mutating func parse(_ config: [String: Any]) {
			objectStorage = config["object_storage"] as? String
			path = config["path"] as? String
			pullInterval = .parse(config["pull_interval"] as? String ?? "5m")
		}
	}
	
	public struct Zone: Sendable {
		public var name: String
		public var offset: Int
	}
}
