import Fluent
import Foundation
import Redis
import Vapor

public struct Resource {
	public typealias DatabaseBuilder = (_ config: Resource.DatabaseConfig) throws -> DatabaseConfiguration
	public typealias StorageBuilder = (_ source: ObjectStorageSource) async throws -> ObjectStorageProvider

	public internal(set) static var shared = Resource()
	
	public internal(set) var groups: [String: Group] = [:]
	
	var storageBuilder: StorageBuilder?

	mutating func setup(
		_ config: Config,
		application: Application,
		databaseBuilder: Resource.DatabaseBuilder?,
		storageBuilder: StorageBuilder?
	) async throws {
		for it in config.groups {
			groups[it.key] = try await .init(it.value, application: application, databaseBuilder: databaseBuilder, storageBuilder: storageBuilder)
		}
		self.storageBuilder = storageBuilder
	}
	
	public func buildStorage(source: ObjectStorageSource) async throws -> ObjectStorageProvider {
		guard let builder = storageBuilder else {
			throw Errors.serverConfigInvalid.wrap("no storageBuilder provide")
		}
		return try await builder(source)
	}

	public var defaultGroup: Group? { groups[Keys.default] }
	
	public struct Group {
		public private(set) var databases: [String: Database] = [:]
		public private(set) var redis: [String: Application.Redis] = [:]
		public private(set) var objectStorages: [String: ObjectStorageProvider] = [:]
		
		let config: GroupConfig
		
		init(_ config: GroupConfig,
			 application: Application,
			 databaseBuilder: Resource.DatabaseBuilder?,
			 storageBuilder: StorageBuilder?) async throws {
			for it in config.databases {
				guard let builder = databaseBuilder else {
					throw AnyError("no databaseBuilder given but configured databases")
				}
				databases[it.key] = try it.value.connect(application: application, isDefault: false, databaseBuilder: builder)
			}
			for it in config.redis {
				redis[it.key] = try it.value.connect(application: application)
			}
			for it in config.objectStorages {
				guard let builder = storageBuilder else {
					throw AnyError("no storageBuilder given but configured storages")
				}
				objectStorages[it.key] = try await builder(it.value)
			}
			self.config = config
		}
	}
	
	struct Config {
		var groups: [String: GroupConfig] = [:]

		init(_ config: [String: Any]) throws {
			for it in config {
				guard  let cfg = it.value as? [String: Any] else {
					throw AnyError("group(\(it.key)) should be dictionary")
				}
				do {
					groups[it.key] = try .init(id: it.key, cfg)
				} catch {
					throw AnyError("group(\(it.key)) invalid", wrap: error)
				}
			}
		}
	}
	
	struct GroupConfig {
		let id: String
		var databases: [String: DatabaseConfig] = [:]
		var redis: [String: RedisConfig] = [:]
		var objectStorages: [String: ObjectStorageSource] = [:]

		init(id: String, _ config: [String: Any]) throws {
			if let cfg = config["database"] as? [String: Any] {
				for it in cfg {
					guard let cfgDB = it.value as? [String: Any] else {
						throw AnyError("database(\(it.key)) should be dictionary")
					}
					do {
						databases[it.key] = try .init(id: it.key, cfgDB)
					} catch {
						throw AnyError("database(\(it.key)) invalid", wrap: error)
					}
				}
			}
			if let cfg = config["redis"] as? [String: Any] {
				for it in cfg {
					guard let cfgDB = it.value as? [String: Any] else {
						throw AnyError("redis(\(it.key)) should be dictionary")
					}
					do {
						redis[it.key] = try .init(id: it.key, cfgDB)
					} catch {
						throw AnyError("redis(\(it.key)) invalid", wrap: error)
					}
				}
			}
			if let cfg = config["object_storage"] as? [String: Any] {
				for it in cfg {
					let source: ObjectStorageSource
					do {
						switch it.value {
						case let cfgDB as [String: String]:
							source = try .init(cfgDB)
						case let url as String:
							source = try .init(urlString: url)
						default:
							throw AnyError("object_storage(\(it.key)) should be dictionary")
						}
					} catch {
						throw AnyError("storage(\(it.key)) invalid", wrap: error)
					}
					objectStorages[it.key] = source
				}
			}
			self.id = id
		}
	}
	
	public struct ObjectStorageConfig {
		let id: String
		public var rawData: [String: Any]
		
	}
	
	public struct DatabaseConfig {
		let id: String
		public var url: URL
		public var rawData: [String: Any]
		
		init(id: String, _ config: [String: Any]) throws {
			guard let value = config[Keys.url] as? String,
				  let url = URL(string: value)
			else { throw AnyError("url invalid: '\(config[Keys.url] as Any)'") }
			self.url = url
			self.id = id
			rawData = config
		}

		func connect(application: Application, isDefault: Bool, databaseBuilder: Resource.DatabaseBuilder) throws -> Database {
			let dbcfg = try databaseBuilder(self)
			let dbID = DatabaseID(string: id)
			application.databases.use(dbcfg, as: dbID, isDefault: isDefault)
			guard let db = application.databases.database(dbID, logger: application.logger, on: application.eventLoopGroup.any())
			else { throw AnyError("fetch database failed") }
			return db
		}
	}
	
	struct RedisConfig {
		let id: String
		var url: URL
		var pool = RedisConfiguration.PoolOptions()

		init(id: String, _ config: [String: Any]) throws {
			guard let value = config[Keys.url] as? String,
				  let url = URL(string: value)
			else { throw AnyError("url invalid: '\(config[Keys.url] as Any)'") }
			self.url = url
			self.id = id
			if let value = config["pool"] as? [String: Any] {
				if let count = config["max_active_conns"] as? Int {
					pool.maximumConnectionCount = .maximumActiveConnections(count)
				} else if let count = config["max_preserved_conns"] as? Int {
					pool.maximumConnectionCount = .maximumPreservedConnections(count)
				}
				if let count = value["min_idle_conns"] as? Int {
					pool.minimumConnectionCount = count
				}
			}
		}
		
		func connect(application: Application) throws -> Application.Redis {
			let redis = application.redis(.init(id))
			redis.configuration = try RedisConfiguration(url: url, pool: pool)
			return redis
		}
	}
}
