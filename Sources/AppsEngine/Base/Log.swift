import Foundation
import Logging

public protocol LogOutputer {
	var level: Log.Level { get }
	func log(_ log: Log)
}

public struct Logger {

	public var outputers: [LogOutputer]
	var trace: Trace
	
	public init(outputers: [LogOutputer] = [], trace: Trace = Trace(nil, on: .zero)) {
		self.outputers = outputers
		self.trace = trace
	}
	
	public mutating func append(trace: Trace) {
		self.trace.merged(trace)
	}
	
	public func with(trace: Trace) -> Self {
		var inst = self
		inst.append(trace: trace)
		return inst
	}
	
	public mutating func append(trace pairs: [Log.Pair]) {
		trace.pairs.append(contentsOf: pairs)
	}
	
	public func with(trace pairs: [Log.Pair]) -> Self {
		var inst = self
		inst.append(trace: pairs)
		return inst
	}

	public func log(_ level: Log.Level, _ subject: String, _ pairs: Log.Pair...) {
		log(Log(level: level, subject, produce(pairs: pairs)))
	}
	
	public func log(_ log: Log) {
		for outputer in marchLevelOutputers(log.level) {
			outputer.log(log)
		}
	}
	
	private func produce(pairs: [Log.Pair]) -> [Log.Pair] {
		if trace.pairs.isEmpty {
			return pairs
		} else if pairs.isEmpty {
			return trace.pairs
		}
		var dedup: [String: Any] = [:]
		dedup.reserveCapacity(trace.pairs.count + pairs.count + 1)
		for log in trace.pairs {
			dedup[log.key] = log.value
		}
		if !trace.startTime.isZero {
			dedup[Keys.traceDuration] = TimeDuration.since(trace.startTime).description
		}
		for v in pairs {
			if let exists = dedup[v.key] {
				dedup[v.key] = "\(exists),\(v.value)"
			} else {
				dedup[v.key] = v.value
			}
		}
		var toLog: [Log.Pair] = []
		toLog.reserveCapacity(dedup.count)
		for (k, v) in dedup {
			toLog.append(.init(k, v))
		}
		return toLog
	}
	
	private func marchLevelOutputers(_ level: Log.Level) -> [LogOutputer] {
		var outputers = [LogOutputer]()
		for outputer in self.outputers {
			if level.index.rawValue >= outputer.level.index.rawValue {
				outputers.append(outputer)
			}
		}
		return outputers
	}
	
	public struct Trace {
		var startTime: Time
		var pairs: [Log.Pair]
		
		public init(_ traceID: String? = nil, on time: Time, _ pairs: [Log.Pair] = []) {
			startTime = time
			self.pairs = pairs
			if let traceID = traceID {
				self.pairs.append(.init(Keys.traceID, traceID))
			}
		}
		
		mutating func merged(_ other: Self) {
			if startTime.isZero {
				startTime = other.startTime
			}
			pairs.append(contentsOf: other.pairs)
		}
	}
}

public struct Log {
	fileprivate enum LevelIndex: UInt8 {
		case debug, info, warn, error
	}
	public enum Level: String, Comparable, Equatable {
		public static func < (lhs: Log.Level, rhs: Log.Level) -> Bool {
			lhs.index.rawValue < rhs.index.rawValue
		}
		
		case debug, info, warn, error
		
		fileprivate var index: LevelIndex {
			switch self {
			case .debug: return .debug
			case .info: return .info
			case .warn: return .warn
			case .error: return .error
			}
		}
		
		public var levelForLogging: Logging.Logger.Level {
			switch self {
			case .debug:
				return .debug
			case .info:
				return .info
			case .warn:
				return .warning
			case .error:
				return .error
			}
		}
	}

	public struct Pair {
		public static func error(_ value: Any) -> Self {
			.init("error", value)
		}
		
		public static func appID(_ value: String) -> Self {
			.init(Keys.appID, value)
		}
		
		public static func userID(_ value: String) -> Self {
			.init(Keys.userID, value)
		}
		
		public var key: String
		public var value: Any
		
		public init(_ key: String, _ value: Any) {
			self.key = key
			self.value = value
		}
		
		var encodedValue: Any {
			switch value {
			case is String, is Bool:
				return value
			default:
				return isNumeric(value) ? value : "\(value)"
			}
		}
	}

	public let level: Level
	public let subject: String
	public var pairs: [Pair] = []
	public let time: Time

	public init(level: Level, _ subject: String, _ pairs: [Pair]) {
		self.level = level
		self.subject = subject
		self.pairs.append(contentsOf: pairs)
		time = .local
	}

	public mutating func append(contentsOf dict: [AnyHashable: Any]) {
		for it in dict {
			pairs.append(.init(it.key as? String ?? it.key.description, "\(it.value)"))
		}
	}
	
	public var encoded: [Pair] {
		var m: [Pair] = [
			.init("ts", RFC3339.shared.string(from: time.asDate, offset: ServerConfig.shared.timezone.offset)),
			.init("level", level.rawValue),
			.init("msg", subject),
		]
		m.append(contentsOf: pairs)
		return m
	}
	
	public var json: Data {
		var s = Self.jsonBrLeft
		var first = true
		for pair in encoded {
			if first {
				first = false
			} else {
				s.append(Self.jsonComma)
			}
			do {
				let key = try JSONSerialization.data(withJSONObject: pair.key, options: Log.jsonOptions)
				let value = try JSONSerialization.data(withJSONObject: pair.encodedValue, options: Log.jsonOptions)
				s.append(key)
				s.append(Self.jsonColon)
				s.append(value)
			} catch {}
		}
		s.append(Log.jsonBrRight)
		return s
	}
	
	private static let jsonBrLeft = Data("{".utf8)
	private static let jsonBrRight = Data("}".utf8)
	private static let jsonColon = Data(":".utf8)
	private static let jsonComma = Data(",".utf8)
	private static let jsonOptions: JSONSerialization.WritingOptions = [
		.fragmentsAllowed,
		.withoutEscapingSlashes,
	]
}
