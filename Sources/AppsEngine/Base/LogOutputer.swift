import Foundation
import Vapor
import NIO

struct LogConsoleOutputer: LogOutputer {
	enum Stream: String {
		case stdout, stderr
		var stream: FileHandle {
			switch self {
			case .stdout:
				return .standardOutput
			case .stderr:
				return .standardError
			}
		}
	}

	let level: Log.Level
	let stream: FileHandle
	
	init(level: Log.Level, stream: Stream) {
		self.level = level
		self.stream = stream.stream
	}
	
	func log(_ log: Log) {
		let data = log.json
		try? stream.write(contentsOf: data)
		try? stream.write(contentsOf: Data("\n".utf8))
	}
}

struct LogVaporOutputer: LogOutputer {
	let level: Log.Level
	let logger: Vapor.Logger
	
	init(level: Log.Level, name: String) {
		self.level = level
		logger = .init(label: name)
	}
	
	func log(_ log: Log) {
		logger.log(level: log.level.vaporLevel, .init(stringLiteral: String(decoding: log.json, as: UTF8.self)), metadata: nil)
	}
}

private extension Log.Level {
	var vaporLevel: Vapor.Logger.Level {
		switch self {
		case .debug: return .debug
		case .info: return .info
		case .warn: return .warning
		case .error: return .error
		}
	}
}

class LogTCPOutputer: LogOutputer {
	let level: Log.Level
	let client: TCPClient
	
	init(level: Log.Level, options: TCPClient.Config) {
		self.level = level
		client = .init(group: .init(numberOfThreads: 1), config: options)
		do {
			_ = try client.connect().wait()
		} catch {
			print("\(self) connect failed: \(error)")
		}
	}
	
	func log(_ log: Log) {
		// FIXME: result cannot received
		_ = client.send(log.json).map { res in
			switch res {
			case .success(_):
				break
			case .failure(let err):
				print("\(self) send failed", err)
			}
		}
	}
}
