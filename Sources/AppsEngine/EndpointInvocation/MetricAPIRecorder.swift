import Foundation

public protocol APIMetricRecorder {
	func record(to ctx: RequestContext, response: HTTPResponse)
}

public struct DefaultAPIMetricRecorder: APIMetricRecorder {
	public func record(to ctx: RequestContext, response: HTTPResponse) {
		let status = metricAPIStatus(code: response.status.code)
		let timecost = TimeDuration.since(ctx.context.startTime)
		let appName = ctx.context.config.metricName
		metricAPIAll(appName: appName, status: status, duration: timecost)
		metricAPIEndpoint(appName: appName, endpoint: ctx.endpoint.name, status: status, duration: timecost)
		if let err = ctx.context.get(WrappedRequestError.self) {
			Metric.shared.count("\(appName).api.\(ctx.endpoint.name).failed.\(status).\(err.error.base.name)")
		}
	}

	private func metricAPIStatus(code: UInt) -> String {
		switch code {
		case 0 ..< 400:
			return "ok"
		case 400 ..< 500:
			return "4xx"
		default:
			return String(code)
		}
	}

	private func metricAPIAll(appName: String, status: String, duration: TimeDuration) {
		// count: {app_name}.api.all
		Metric.shared.count("\(appName).api.all")
		if status == "ok" {
			// time cost: {app_name}.api.all.timecost
			Metric.shared.timer("\(appName).api.all.ok.timecost", duration: duration.seconds)
		}
		// count: {app_name}.api.{suffix}.[ok|4xx|5xx]
		Metric.shared.count("\(appName).api.all.\(status)")
	}

	/// record API metric with `appName` for `endpoint` and `all`.
	private func metricAPIEndpoint(appName: String, endpoint: String, status: String, duration: TimeDuration) {
		let endpoint = endpoint.isEmpty ? "__not_found__" : endpoint
		for suffix in ["all", endpoint] {
			// count: {app_name}.api.endpoint.[all|{endpoint}]
			Metric.shared.count("\(appName).api.endpoint.\(suffix)")
			// count: {app_name}.api.endpoint.[all|{endpoint}].[ok|4xx|5xx]
			Metric.shared.count("\(appName).api.endpoint.\(suffix).\(status)")
			if status == "ok" {
				// time cost: {app_name}.api.endpoint.[all|{endpoint}].ok.timecost
				Metric.shared.timer("\(appName).api.endpoint.\(suffix).ok.timecost", duration: duration.seconds)
			}
		}
	}
}
