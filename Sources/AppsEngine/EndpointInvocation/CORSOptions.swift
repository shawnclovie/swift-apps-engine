import NIOHTTP1
import Vapor

/// Configuration used for populating headers in response for CORS requests.
public struct CORSOptions {
	public typealias AllowOriginSetting = CORSMiddleware.AllowOriginSetting

	public static let defaultAllowedMethods: [HTTPMethod] = [
		.GET, .POST, .PUT, .HEAD, .DELETE, .PATCH,
	]

	public static let defaultAllowedHeaders: [HTTPHeaders.Name] = [
		.accept, .authorization, .contentType, .origin, .xRequestedWith,
	]

	public static let defaultCacheExpiration = 3600

	/// Setting that controls which origin values are allowed.
	public var allowedOrigin: CORSMiddleware.AllowOriginSetting

	/// Header string containing methods that are allowed for a CORS request response.
	public var allowedMethods: [HTTPMethod]

	/// Header string containing headers that are allowed in a response for CORS request.
	public var allowedHeaders: [HTTPHeaders.Name]

	/// If set to yes, cookies and other credentials will be sent in the response for CORS request.
	public var allowCredentials: Bool

	/// Optionally sets expiration of the cached pre-flight request. Value is in seconds.
	public var cacheExpiration: Int?

	/// Headers exposed in the response of pre-flight request.
	public var exposedHeaders: [HTTPHeaders.Name]?

	/// Instantiate a CORSConfiguration struct that can be used to create a `CORSConfiguration`
	/// middleware for adding support for CORS in your responses.
	///
	/// - parameters:
	///   - allowedOrigin: Setting that controls which origin values are allowed.
	///   - allowedMethods: Methods that are allowed for a CORS request response.
	///   - allowedHeaders: Headers that are allowed in a response for CORS request.
	///   - allowCredentials: If cookies and other credentials will be sent in the response.
	///   - cacheExpiration: Optionally sets expiration of the cached pre-flight request in seconds.
	///   - exposedHeaders: Headers exposed in the response of pre-flight request.
	public init(
		allowedOrigin: AllowOriginSetting = .originBased,
		allowedMethods: [HTTPMethod] = defaultAllowedMethods,
		allowedHeaders: [HTTPHeaders.Name] = defaultAllowedHeaders,
		allowCredentials: Bool = true,
		cacheExpiration: Int? = defaultCacheExpiration,
		exposedHeaders: [HTTPHeaders.Name]? = nil
	) {
		self.allowedOrigin = allowedOrigin
		self.allowedMethods = allowedMethods
		self.allowedHeaders = allowedHeaders
		self.allowCredentials = allowCredentials
		self.cacheExpiration = cacheExpiration
		self.exposedHeaders = exposedHeaders
	}
	
	public init(from encoded: [String: Any]) {
		allowedOrigin = Self.decodeAllowOriginSetting(from: encoded[Keys.allowedOrigin])
		allowedMethods = Self.decode(methods: encoded[Keys.allowedMethods])
			?? Self.defaultAllowedMethods
		var headers = Self.decode(headers: encoded[Keys.allowedHeaders])
			?? Self.defaultAllowedHeaders
		if let extra = Self.decode(headers: encoded[Keys.extraAllowedHeaders]) {
			headers += extra
		}
		allowedHeaders = headers
		allowCredentials = anyToBool(encoded[Keys.allowCredentials])
			?? true
		cacheExpiration = anyToInt64(encoded[Keys.cacheExpiration]).map(Int.init)
			?? Self.defaultCacheExpiration
		exposedHeaders = Self.decode(headers: encoded[Keys.exposedHeaders])
	}
	
	public var encoded: [String: Any] {
		var encoded: [String: Any] = [
			Keys.allowedOrigin: Self.encode(allowedOrigin),
			Keys.allowedMethods: Self.encode(methods: allowedMethods),
			Keys.allowedHeaders: Self.encode(headers: allowedHeaders),
			Keys.allowCredentials: allowCredentials,
		]
		if let cacheExpiration = cacheExpiration {
			encoded[Keys.cacheExpiration] = cacheExpiration
		}
		if let exposedHeaders = exposedHeaders {
			encoded[Keys.exposedHeaders] = Self.encode(headers: exposedHeaders)
		}
		return encoded
	}

	public var configuration: CORSMiddleware.Configuration {
		.init(allowedOrigin: allowedOrigin, allowedMethods: allowedMethods, allowedHeaders: allowedHeaders, allowCredentials: allowCredentials, cacheExpiration: cacheExpiration, exposedHeaders: exposedHeaders)
	}

	public static func decodeAllowOriginSetting(from: Any?) -> AllowOriginSetting {
		switch from {
		case nil:					return .originBased
		case let str as String:
			switch str {
			case "origin_based":	return .originBased
			case "all", "*":		return .all
			case "none":			return .none
			default:				return .custom(str)
			}
		case let vs as [String]:	return .any(.init(vs))
		default:					return .none
		}
	}
	
	public static func encode(_ settings: AllowOriginSetting) -> Any {
		switch settings {
		case .originBased:			return "origin_based"
		case .none:					return "none"
		case .all:					return "all"
		case .any(let vs):			return [String](vs)
		case .custom(let v):		return v
		}
	}

	private static func decode(headers: Any?) -> [HTTPHeaders.Name]? {
		switch headers {
		case let v as String:
			return v.split(separator: ",").map(Self.decode(header:))
		case let vs as [String]:
			return vs.map(Self.decode(header:))
		default:
			return nil
		}
	}

	private static func decode<T>(header: T) -> HTTPHeaders.Name
	where T: StringProtocol {
		.init(header.trimmingCharacters(in: .whitespaces).uppercased())
	}

	private static func decode(methods: Any?) -> [HTTPMethod]? {
		switch methods {
		case let v as String:
			return v.split(separator: ",").map(Self.decode(method:))
		case let vs as [String]:
			return vs.map(Self.decode(method:))
		default:
			return nil
		}
	}

	private static func decode<T>(method: T) -> HTTPMethod
	where T: StringProtocol {
		.init(rawValue: method.trimmingCharacters(in: .whitespaces).uppercased())
	}

	private static func encode(methods: [HTTPMethod]) -> String {
		methods.map({ "\($0)" }).joined(separator: ", ")
	}

	private static func encode(headers: [HTTPHeaders.Name]) -> String {
		headers.map({ String(describing: $0) }).joined(separator: ", ")
	}
}
