import Foundation
import Vapor

public final class EngineApp {
	public let rootPath: URL
	public let predefinedEndpoints: Endpoint.Register

	public internal(set) var config: AppConfigSet
	let configWarnings: [String: [String: Any]]

	private var routesToRegister: [RouteRef] = []
	private var namedRoutes: [String: RouteRef] = [:]
	private var endpointNamedRoutes: [String: RouteRef] = [:]

	private var routes = Routes()
	private let router: TrieRouter<CachedRoute> = .init()

	/// Server wide middlewares.
	private var middleware: [Middleware] = []
	private var notFoundResponder: Responder = NotFoundResponder()
	let requestProcessor: RequestProcessor?
	let apiMetric: APIMetricRecorder?

	init(path: URL, predefinedEndpoints: Endpoint.Register,
		 modules: [Module],
	     requestProcessor: RequestProcessor?,
		 apiMetric: APIMetricRecorder?
	) throws {
		guard FileManager.default.fileExists(atPath: path.path),
		      try path.resourceValues(forKeys: [.isDirectoryKey]).isDirectory == true
		else {
			throw Errors.appTestFailed.wrap("app path(\(path)) should be directory")
		}
		rootPath = path
		self.predefinedEndpoints = predefinedEndpoints
		config = try .init(rootPath: path)

		// load app config
		configWarnings = config.load(for: modules)
		if !configWarnings.isEmpty {
			Metric.shared.count("\(config.metricName).app.new_app.warning", count: 1)
		}
		self.requestProcessor = requestProcessor
		self.apiMetric = apiMetric

		let warningFile = path.appendingPathComponent("warning.json")
		if let report = configWarningsReport {
			try? report.write(to: warningFile)
		} else {
			try? FileManager.default.removeItem(at: warningFile)
		}
	}

	public var appID: String { config.core.appID }

	var configWarningsReport: Data? {
		guard !configWarnings.isEmpty else { return nil }
		return try? JSONSerialization.data(withJSONObject: configWarnings, options: [.prettyPrinted])
	}

	func prepare(middlewares: [Middleware]) {
		if let opts = config.core.cors {
			middleware.append(CORSMiddleware(configuration: opts.configuration))
		}
		for ep in predefinedEndpoints.endpoints.values {
			routesToRegister.append(contentsOf: makeRoutes(endpoint: ep))
		}
		middleware.append(contentsOf: middlewares)
		namedRoutes.reserveCapacity(routesToRegister.count)
		endpointNamedRoutes.reserveCapacity(routesToRegister.count)
		let verboseRoute = Engine.verboseItems.contains("route")
		for route in routesToRegister {
			namedRoutes[route.name] = route
			endpointNamedRoutes[route.endpoint.name] = route
			register(logger: verboseRoute ? .startup : nil, route)
		}
		notFoundResponder = middleware.makeResponder(chainingTo: notFoundResponder)
	}

	private func makeRoutes(endpoint: Endpoint) -> [RouteRef] {
		let paths = endpoint.route.path.split(separator: "/").compactMap {
			$0.isEmpty ? nil : PathComponent(stringLiteral: String($0))
		}
		switch endpoint.invocation {
		case .webSocket(_):
			let route = routes.webSocket(paths) { (request, ws) async in
				await self.serve(to: request, webSocket: ws, endpoint: endpoint)
			}
			return [.init(route: route, endpoint: endpoint)]
		case .request(_):
			return endpoint.route.method.map { method in
				let route = routes.on(method, paths, body: .collect) { (request) async in
					await self.serve(to: request, endpoint: endpoint)
				}
				return .init(route: route, endpoint: endpoint)
			}
		}
	}

	func dispose() {
	}
}

extension EngineApp {
	private func readBody(from request: Request, _ ctx: Context) -> ByteBuffer? {
		if let requestProcessor = requestProcessor {
			return requestProcessor.processRequest(ctx, request)
		}
		return request.body.data
	}
	
	private func serve(to request: Request, webSocket: WebSocket, endpoint: Endpoint) async {
		let ctx = Context(configSet: config)
		let reqCTX = RequestContext(request: request, context: ctx, endpoint: endpoint, body: readBody(from: request, ctx))
		await reqCTX.serve(webSocket: webSocket)
	}

	private func serve(to request: Request, endpoint: Endpoint) async -> Response {
		let ctx = Context(configSet: config)
		// decode request body if needed
		let reqCTX = RequestContext(request: request, context: ctx, endpoint: endpoint, body: readBody(from: request, ctx))
		var response = await reqCTX.serve()
		// encode response body if needed
		if let requestProcessor = requestProcessor {
			response = requestProcessor.processResopnse(ctx, response)
		}
		apiMetric?.record(to: reqCTX, response: response)
		if let err = response.error as? WrapError, [.database, .internal].contains(err.base) {
			ctx.logger.log(.error, "runtime_error", .init("url", request.url), .error(err.descriptionWithCallerStack))
		}
		return response.response
	}

	private func serveShadowRoute(to request: Request, endpoint: Endpoint) async -> Response {
		let reqCTX = RequestContext(
			request: request, context: .init(configSet: config),
			endpoint: endpoint, body: request.body.data)
		let response = await reqCTX.serve()
		return response.response
	}
}

extension EngineApp: Responder {
	private func register(logger: Logger?, _ ref: RouteRef) {
		// Make a copy of the route to cache middleware chaining.
		let cached = CachedRoute(route: ref,
								 responder: middleware.makeResponder(chainingTo: ref.route.responder),
								 isShadowed: false)
		let paths = ref.nonEmptyPathComponents
		var shadowResponders: [String: Responder] = [:]
		if ref.shouldRegisterShadowHeadRoute {
			shadowResponders[HTTPMethod.HEAD.string] = OKResponder()
		}
		for mw in ref.endpoint.middlewares {
			for method in mw.shadowRouteMethods
			where !ref.endpoint.route.method.contains(method) {
				var ep = ref.endpoint
				ep.invocation = .request(OKResponder())
				shadowResponders[method.string] = AsyncBasicResponder(closure: { (request) async in
					await self.serveShadowRoute(to: request, endpoint: ep)
				})
			}
		}
		for (method, responder) in shadowResponders {
			registerShadowRoute(logger: logger, method: .init(rawValue: method), responder: responder, cached: cached, paths: paths)
		}
		logger?.log(.debug, "\(type(of: self)) register",
					.init("route", "\(ref.route.method) \(paths.string)"),
					.init("endpoint", ref.endpoint.name))
		router.register(cached, at: [.constant(ref.route.method.string)] + paths)
	}
	
	private func registerShadowRoute(logger: Logger?, method: HTTPMethod, responder: Responder, cached: CachedRoute, paths: [PathComponent]) {
		let route = cached.route.route
		let shadowed = Vapor.Route(method: method,
			path: route.path,
			responder: middleware.makeResponder(chainingTo: responder),
			requestType: route.requestType,
			responseType: route.responseType)
		let _cached = CachedRoute(
			route: .init(route: shadowed, endpoint: cached.route.endpoint),
			responder: middleware.makeResponder(chainingTo: responder),
			isShadowed: true)
		logger?.log(.debug, "\(type(of: self)) register",
					.init("route", "\(method) \(paths.string) (shadowed)"))
		router.register(_cached, at: [.constant(method.string)] + paths)
	}

	public func respond(to request: Request) -> EventLoopFuture<Response> {
		let response: EventLoopFuture<Response>
		if let cachedRoute = getRoute(for: request) {
			request.route = cachedRoute.route.route
			response = cachedRoute.responder.respond(to: request)
		} else {
			response = notFoundResponder.respond(to: request)
		}
		return response
	}

	/// Gets a `Route` from the underlying `TrieRouter`.
	private func getRoute(for request: Request) -> CachedRoute? {
		let pathComponents = request.url.path
			.split(separator: "/")
			.map(String.init)
		// If it's a HEAD request and a HEAD route exists, return that route...
		if request.method == .HEAD,
		   let route = router.route(
			path: [HTTPMethod.HEAD.string] + pathComponents,
			parameters: &request.parameters) {
			return route
		}
		// ...otherwise forward HEAD requests to GET route
		let method = (request.method == .HEAD) ? .GET : request.method
		return router.route(
			path: [method.string] + pathComponents,
			parameters: &request.parameters)
	}
}

/// For runtime respond.
private struct CachedRoute {
	let route: RouteRef
	let responder: Responder
	let isShadowed: Bool
}

/// For define relation between `endpoint` and `route`.
private struct RouteRef {
	var route: Vapor.Route
	var endpoint: Endpoint
	
	var name: String { "\(route.method.string) \(endpoint.route.path)" }

	/// Remove any empty path components.
	var nonEmptyPathComponents: [PathComponent] {
		route.path.filter { component in
			if case let .constant(string) = component {
				return string != ""
			}
			return true
		}
	}

	/// If the route isn't explicitly a HEAD route and it's made up solely of .constant components,
	/// register a HEAD route with the same path.
	var shouldRegisterShadowHeadRoute: Bool {
		route.method == .GET && route.path.allSatisfy({ component in
			if case .constant = component { return true }
			return false
		})
	}
}

private struct OKResponder: Responder, RequestInvocation {
	func respond(to request: Request) -> EventLoopFuture<Response> {
		request.eventLoop.makeSucceededFuture(.init(status: .ok))
	}
	
	func respond(to ctx: RequestContext) async throws -> HTTPResponse {
		.empty(.ok)
	}
}

private struct NotFoundResponder: Responder {
	func respond(to request: Request) -> EventLoopFuture<Response> {
		request.eventLoop.future(HTTPResponse.error(Errors.routeNotFound).response)
	}
}

/// For Vapor
extension HTTPResponse {
	init(response: Response) {
		self.init(response.status, headers: response.headers, body: response.body.buffer)
	}
	
	var response: Response {
		.init(status: status,
			  headers: headers,
			  body: body.map({.init(buffer: $0)}) ?? .empty)
	}
}
